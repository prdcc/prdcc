/*
 * File:   main.cpp
 * Author: rtucker
 *
 * Created on 16 March 2015, 11:13
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdbool.h>

/*
 * 
 */
int main(int argc, char** argv) {

    bool loop = true;
    char* msg = (char*)malloc(200);
    while(loop){
        int index = 0;
        memset(msg, 0, 200 * sizeof(char));
        while(read(STDIN_FILENO, &msg[index], 1) > 0){
            index++;
            if(msg[index-1] == '\n'){
                msg[index-1] = '\0';
                index--;
                break;
            }
        }
        if(msg[0] == 'q' && 1 == index){
            loop = false;
        } else {
            //Reverse the message and write it into stdout
            char* msgOut = (char*)malloc(index);
            int i;
            for(i=0; i < index; i++){
                msgOut[i] = msg[index -i -1];
            }
            msgOut[index] = '\n';
            write(STDOUT_FILENO, msgOut, index+1);
            free(msgOut);
        }
    }
    free(msg);

    return 0;
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prdcc.desktop.app.Model;

import java.awt.Component;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import prdcc.desktop.app.PRDCCDesktopApp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *
 * @author Josh
 */
public class DBConnect {
    private Object txt_username;
    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");    
    Date date = new Date(); 
        
    
    public ResultSet DB_Run(String SQL) throws SQLException{
       
       //Data
       Connection con = null;
       String Username = "PRDCC";
       String Password = "Plymouth2014!";
       String Database = "jdbc:oracle:thin:@tom.uopnet.plymouth.ac.uk:1522:ORCL12c";
       System.out.println("Connecting to the database (" + Database + ")");
       
       //Test Connection 
        try {
            con = DriverManager.getConnection(Database, Username, Password );
        } catch (SQLException ex) {
            System.out.println("Database Connection Failed");
           Component frame = null;
            JOptionPane.showMessageDialog(frame,"Database Connection has failed!","Database Connection",JOptionPane.ERROR_MESSAGE);
        
        }
         
        //SQL Query 
        String query = SQL;
         
        java.sql.Statement stmt;
        stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);

        //Return results
        return rs;
         
    }
    
    public String DB_OneString(String SQL) throws SQLException{
       
        //Create output variable 
        String output=null;
        
        //Use the DB_Run method to run a inputed SQL Statment 
        ResultSet rs = DB_Run(SQL);
        
        //Extract the one string from the Results and end the loop
        try {
            while (rs.next()) {
                output = rs.getString(1);
              break;
                
            }
            
        } catch (SQLException ex) {
            System.out.println("data output failed");
        }
        
        //Return single string
         return output;
    }
    
    public Boolean DB_LogIn(String user, String pass){
    
        //Create Login boolean to determin if the login was correct
        Boolean DB_Login = false;
        
        //Create variables and the SQL statment 
        ResultSet data = null;
        String output_user = null;
        String output_pass = null;
        String SQL= "SELECT USERNAME, PASS FROM ADMIN WHERE USERNAME = '"+user+"' AND PASS = '"+pass+"'";
        
        
        
        //Run the SQL using the DB_Run method 
        try {
            data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL caputure failed");
        }
        
        //Get the results from the database, the username is tested twice (on the database and the application)
        try {
            while (data.next()) {
                output_user = data.getString(1);
                output_pass = data.getString(2);      
                
                //If the database results equal the inputed data then return a true boolean
                if(output_user.equals(user) && output_pass.equals(pass)){
                        DB_Login = true;
                        }
                }
            
        } catch (SQLException ex) {
            System.out.println("data output failed");
        }
        
        //Return the boolean 
        return DB_Login;
        
    }
    
    public ArrayList DB_Admin(){
        
        //Create a Temp array for the data
        ArrayList<String> tempAdminArray = new ArrayList<String>();
        
        //Create the variables and the SQL statment 
        ResultSet data = null;
        String SQL= "SELECT USERNAME, ADMINLEVEL, CREATIONDATE FROM ADMIN WHERE USERNAME = '"+PRDCCDesktopApp.username+"' ";
        
        //Run the query using the DB_Run method 
        try {
            data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL caputure failed");;
        }
        
        //Add the data to an array 
        try {
            while (data.next()) {
                tempAdminArray.add(data.getString(1));
                tempAdminArray.add(data.getString(2));
                tempAdminArray.add(data.getString(3));
                break;
            }
            
        } catch (SQLException ex) {
            System.out.println("data output failed");
        }
        
        //Return the array 
        return tempAdminArray;
    }
    
    public ArrayList DB_ToDo(){
        
        //Create a temp array 
        ArrayList<String> tempToDoArray = new ArrayList<String>();
        
        //run the SQL querys using the one string method
        try {   
            //Add the return string to the temp array 
            tempToDoArray.add(DB_OneString("SELECT COUNT(STATUS) AS lol FROM AD WHERE STATUS = 'PENDING'"));
            tempToDoArray.add(DB_OneString("SELECT COUNT(STATUS) AS lol FROM REQUESTS WHERE STATUS = 'PENDING'"));
            tempToDoArray.add(DB_OneString("SELECT COUNT(STATUS) AS lol FROM COMPLAINTS WHERE STATUS = 'PENDING'"));
            
            
        } catch (SQLException ex) {
            System.out.println("FAIL");
        }
        
        //return the temp array 
        return tempToDoArray;
    }
    
    public ArrayList DB_AllUsers(){
        
        //Create a temp array 
        ArrayList<Users> tempAllUsers = new ArrayList<Users>();
        
        ResultSet data = null;
        String SQL= "SELECT * FROM USERS";
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        String username;
        String firstname;
        String surname;
        String email;
        String city;
        String postcode;
        String contactNumber;
        String uni;
        String password;
        int swaps;
        String status;
        
        try{
        while (data.next()) {
                username = (data.getString(1));
                firstname = (data.getString(2));
                surname = (data.getString(3));
                email = (data.getString(4));
                city = (data.getString(5));
                postcode = (data.getString(6));
                contactNumber = (data.getString(7));
                uni = (data.getString(8));
                password = (data.getString(9));
                swaps = (data.getInt(10));
                status = (data.getString(11));
                
                tempAllUsers.add(new Users (username,firstname,surname,email,city,postcode,contactNumber,uni,password,swaps,status));
            }
        } catch (SQLException ex) {
            System.out.println("ArrayList data collection failed");
        }
        
        
        
        //Return the array 
        System.out.println("All Users Found");
        return tempAllUsers;
    }
    
    public ArrayList DB_UpdateUser(String username, String firstname, String surname, String email, String city, String postcode, String contactNumber, String uni, String password, int swaps, String status){
        
        String SQL;
        SQL  = "UPDATE PRDCC.USERS SET ";
        SQL += "USERNAME = '"+username;
        SQL += "', FIRSTNAME = '" + firstname;
        SQL += "', SURNAME = '" + surname;
        SQL += "', EMAIL ='" + email;
        SQL += "', CITY = '" + city;
        SQL += "', POSTCODE = '" + postcode;
        SQL += "', CONTACTNUMBER = " + contactNumber;
        SQL += ", UNI = '" +uni;
        SQL += "', PASSWORD = '" + password;
        SQL += "', BALANCE = " + Integer.toString(swaps);
        SQL += " WHERE USERNAME = '" + username + "'";
        
        ResultSet data = null;
        try{
            data = DB_Run(SQL);
            System.out.println("User Updated");
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        return null;
    }
    
    public ArrayList DB_AllAdmins(){
        
        //Create a temp array 
        ArrayList<Admins> tempAllAdmins = new ArrayList<Admins>();
        
        ResultSet data = null;
        String SQL= "SELECT * FROM ADMIN";
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        String username;
        String password;
        String date;
        int level;
        String comments;
        
        try{
        while (data.next()) {
                username = (data.getString(1));
                password = (data.getString(2));
                date = (data.getString(3));
                level = Integer.parseInt((data.getString(4)));
                comments = (data.getString(5));
                
                
                tempAllAdmins.add(new Admins (username,password,date,level,comments));
            }
        } catch (SQLException ex) {
            System.out.println("ArrayList data collection failed");
        }
        
        
        
        //Return the array 
        System.out.println("All Admin Users Found");
        return tempAllAdmins;
    }
            
    public ArrayList DB_UpdateAdmin(String username, String date, int level, String password, String comments){
        
        String SQL= "UPDATE ADMIN SET PASS = '"+ password +"', ADMINLEVEL = "+ level +", COMMENTS = '"+ comments +"' WHERE USERNAME='"+username+"'";
        
//        UPDATE Customers
//        SET ContactName='Alfred Schmidt', City='Hamburg'
//        WHERE CustomerName='Alfreds Futterkiste';
        
        ResultSet data = null;
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        return null;
    }
    
    public ArrayList DB_AllRequests(){
        
        //Create a temp array 
        ArrayList<Requests> tempAllRequests = new ArrayList<Requests>();
        
        ResultSet data = null;
        String SQL= "SELECT * FROM REQUESTS";
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        int ID;
        String title;
        int price;
        String description;
        String category;
        String subcategory;
        String requester;
        String status;
        String datePosted;
        String dateNeeded;
        String city;
        String postcode;
        int distance;
        String offer;
        
        try{
        while (data.next()) {
                ID =  Integer.parseInt(data.getString(1));
                title = data.getString(2);
                price =  Integer.parseInt(data.getString(3));
                description = data.getString(4);
                category = data.getString(5);
                subcategory = data.getString(6);
                requester = data.getString(7);
                status = data.getString(8);
                datePosted = data.getString(9);
                dateNeeded = data.getString(10);
                city = data.getString(11);
                postcode = data.getString(12);
                distance =  Integer.parseInt(data.getString(13));
                offer = data.getString(14);
                
                tempAllRequests.add(new Requests (ID, title, price, description, category, subcategory, requester, status, datePosted, dateNeeded, city, postcode, distance, offer));
            }
        } catch (SQLException ex) {
            System.out.println("ArrayList data collection failed");
        }
        
        
        
        //Return the array 
        System.out.println("All Requests Found");
        return tempAllRequests;
    }
    
    public ArrayList DB_AllRequestsAPROVAL(){
        
        //Create a temp array 
        ArrayList<Requests> tempAllRequests = new ArrayList<Requests>();
        
        ResultSet data = null;
        String SQL= "SELECT * FROM REQUESTS WHERE STATUS = 'PENDING'";
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        int ID;
        String title;
        int price;
        String description;
        String category;
        String subcategory;
        String requester;
        String status;
        String datePosted;
        String dateNeeded;
        String city;
        String postcode;
        int distance;
        String offer;
        
        try{
        while (data.next()) {
                ID =  Integer.parseInt(data.getString(1));
                title = data.getString(2);
                price =  Integer.parseInt(data.getString(3));
                description = data.getString(4);
                category = data.getString(5);
                subcategory = data.getString(6);
                requester = data.getString(7);
                status = data.getString(8);
                datePosted = data.getString(9);
                dateNeeded = data.getString(10);
                city = data.getString(11);
                postcode = data.getString(12);
                distance =  Integer.parseInt(data.getString(13));
                offer = data.getString(14);
                
                tempAllRequests.add(new Requests (ID, title, price, description, category, subcategory, requester, status, datePosted, dateNeeded, city, postcode, distance, offer));
            }
        } catch (SQLException ex) {
            System.out.println("ArrayList data collection failed");
        }
        
        
        
        //Return the array 
        System.out.println("All Pending Requests Found");
        return tempAllRequests;
    }
    
    public ArrayList DB_UpdateRequest(int ID, String status){
        
        String SQL= "UPDATE REQUESTS SET STATUS = '"+status+"', DATEPOSTED = TO_DATE('"+ dateFormat.format(date) +"','mm/dd/yyyy') WHERE ID='"+ID+"'";
        
        //dateFormat.format(date)
        //TO_DATE('"+date+"','mm/dd/yyyy')
        ResultSet data = null;
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        return null;
    }
     
    public ArrayList DB_AllAdverts(){
        
        //Create a temp array 
        ArrayList<Adverts> tempAllAdverts = new ArrayList<Adverts>();
        
        ResultSet data = null;
        String SQL= "SELECT * FROM AD";
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        int ID;
        String title;
        int price;
        String description;
        String category;
        String subcategory;
        String seller;
        String status;
        String datePosted;
        String dateNeeded;
        String city;
        String postcode;
        int distance;
        
        try{
        while (data.next()) {
                ID =  Integer.parseInt(data.getString(1));
                title = data.getString(2);
                price =  Integer.parseInt(data.getString(3));
                description = data.getString(4);
                category = data.getString(5);
                subcategory = data.getString(6);
                seller = data.getString(7);
                status = data.getString(8);
                datePosted = data.getString(9);
                dateNeeded = data.getString(10);
                city = data.getString(11);
                postcode = data.getString(12);
                distance =  Integer.parseInt(data.getString(13));
                
                tempAllAdverts.add(new Adverts (ID, title, price, description, category, subcategory, seller, status, datePosted, dateNeeded, city, postcode, distance));
            }
        } catch (SQLException ex) {
            System.out.println("ArrayList data collection failed");
        }
        
        
        
        //Return the array 
        System.out.println("All Adverts Found");
        return tempAllAdverts;
    }
    
    public ArrayList DB_UpdateAdverts(int ID, String status){
        
        String SQL= "UPDATE AD SET STATUS = '"+status+"', DATEPOSTED = TO_DATE('"+ dateFormat.format(date) +"','mm/dd/yyyy')  WHERE ID='"+ID+"'";
        
        
        ResultSet data = null;
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        return null;
    }
     
    public ArrayList DB_AllAdvertsAPROVAL(){
        
        //Create a temp array 
        ArrayList<Adverts> tempAllAdverts = new ArrayList<Adverts>();
        
        ResultSet data = null;
        String SQL= "SELECT * FROM AD WHERE STATUS = 'PENDING'";
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        int ID;
        String title;
        int price;
        String description;
        String category;
        String subcategory;
        String seller;
        String status;
        String datePosted;
        String dateNeeded;
        String city;
        String postcode;
        int distance;
        
        try{
        while (data.next()) {
                ID =  Integer.parseInt(data.getString(1));
                title = data.getString(2);
                price =  Integer.parseInt(data.getString(3));
                description = data.getString(4);
                category = data.getString(5);
                subcategory = data.getString(6);
                seller = data.getString(7);
                status = data.getString(8);
                datePosted = data.getString(9);
                dateNeeded = data.getString(10);
                city = data.getString(11);
                postcode = data.getString(12);
                distance =  Integer.parseInt(data.getString(13));
                
                tempAllAdverts.add(new Adverts (ID, title, price, description, category, subcategory, seller, status, datePosted, dateNeeded, city, postcode, distance));
            }
        } catch (SQLException ex) {
            System.out.println("ArrayList data collection failed");
        }
        
        
        
        //Return the array 
        System.out.println("All Pending Advert Found");
        return tempAllAdverts;
    }
    
    public ArrayList DB_AllComplaints(){
        
        //Create a temp array 
        ArrayList<Complaints> tempAllComplaints = new ArrayList<Complaints>();
        
        ResultSet data = null;
        String SQL= "SELECT * FROM COMPLAINTS";
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        int ID;
        String dateSent;
        String status;
        String user;
        int ad;
        int requests;
        String notes;
        
        try{
        while (data.next()) {
                ID =  Integer.parseInt(data.getString(1));
                dateSent = data.getString(2);
                status = data.getString(3);
                user = data.getString(4);
                ad = Integer.parseInt(data.getString(5));
                requests = Integer.parseInt(data.getString(6));
                notes = data.getString(7);
                
                tempAllComplaints.add(new Complaints (ID, dateSent, status, user, ad, requests, notes));
            }
        } catch (SQLException ex) {
            System.out.println("ArrayList data collection failed");
        }
        
        
        
        //Return the array 
        System.out.println("All Complaints Found");
        return tempAllComplaints;
    }
    
    public ArrayList DB_UpdateComplaints(int ID, String status, String notes){
        
        String SQL = "UPDATE COMPLAINTS SET STATUS = '"+status+"', NOTES = '"+notes+"' WHERE ID = "+String.valueOf(ID);
        
        
        ResultSet data = null;
        try{
        data = DB_Run(SQL);
        System.out.println("Complaint Updated");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
     
    public ArrayList DB_AllComplaintsAPROVAL(){
        
        //Create a temp array 
        ArrayList<Complaints> tempAllComplaints = new ArrayList<Complaints>();
        
        ResultSet data = null;
        String SQL= "SELECT * FROM COMPLAINTS WHERE STATUS = 'PENDING'";
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        int ID;
        String dateSent;
        String status;
        String user;
        int ad;
        int requests;
        String notes;
        
        try{
        while (data.next()) {
                ID =  Integer.parseInt(data.getString(1));
                dateSent = data.getString(2);
                status = data.getString(3);
                user = data.getString(4);
                ad = Integer.parseInt(data.getString(5));
                requests = Integer.parseInt(data.getString(6));
                notes = data.getString(7);
                
                
                tempAllComplaints.add(new Complaints (ID, dateSent, status, user, ad, requests, notes));
            }
        } catch (SQLException ex) {
            System.out.println("ArrayList data collection failed");
        }
        
        
        
        //Return the array 
        System.out.println("All Pending Complaints Found");
        return tempAllComplaints;
    }
    
    public ArrayList DB_AllUsersAPROVAL(){
        
        //Create a temp array 
        ArrayList<Users> tempAllUsers = new ArrayList<Users>();
        
        ResultSet data = null;
        String SQL= "SELECT * FROM USERS WHERE STATUS = 'Pending'";
        try{
        data = DB_Run(SQL);
        } catch (SQLException ex) {
            System.out.println("SQL Fail");
        }
        
        String username;
        String firstname;
        String surname;
        String email;
        String city;
        String postcode;
        String contactNumber;
        String uni;
        String password;
        int swaps;
        String status;
        
        try{
        while (data.next()) {
                username = (data.getString(1));
                firstname = (data.getString(2));
                surname = (data.getString(3));
                email = (data.getString(4));
                city = (data.getString(5));
                postcode = (data.getString(6));
                contactNumber = (data.getString(7));
                uni = (data.getString(8));
                password = (data.getString(9));
                swaps = (data.getInt(10));
                status = (data.getString(11));
                
                
                tempAllUsers.add(new Users (username,firstname,surname,email,city,postcode,contactNumber,uni,password,swaps,status));
            }
        } catch (SQLException ex) {
            System.out.println("ArrayList data collection failed");
        }
        
        
        
        //Return the array 
        System.out.println("All Pending Users Found");
        return tempAllUsers;
    }
    
    public String DB_NewAdmin(String username, String date, int level, String password, String comments) throws SQLException{
       
        //Create output variable 
        String output=null;
        
        //SQL String
        String SQL="INSERT INTO \"PRDCC\".\"ADMIN\" (USERNAME, PASS, CREATIONDATE, ADMINLEVEL, COMMENTS) VALUES ('"+username+"', '"+password+"', TO_DATE('"+date+"','mm/dd/yyyy'), '"+level+"', '"+comments+"')";
        //Use the DB_Run method to run a inputed SQL Statment 
        ResultSet rs = DB_Run(SQL);
        Component frame = null;        
        JOptionPane.showMessageDialog(frame,"Admin User: "+username+" has been added");
        System.out.println("ADmin added");
        
        //Return single string
         return null;
    }
    
    public String DB_DeleteAdmin(String username) throws SQLException{
       
        //Create output variable 
        String output=null;
        
        //SQL String
        String SQL="DELETE FROM ADMIN WHERE USERNAME ='"+username+"'";

        //Use the DB_Run method to run a inputed SQL Statment 
        ResultSet rs = DB_Run(SQL);
        Component frame = null;        
        JOptionPane.showMessageDialog(frame,"Admin User: "+username+" has been Deleted");
        System.out.println("Admin deleted");
        
        //Return single string
         return null;
    }
    
    public String DB_DeleteUser(String username) throws SQLException{
       
        //Create output variable 
        String output=null;
        
        //SQL String
        String SQL="DELETE FROM USERS WHERE USERNAME ='"+username+"'";

        //Use the DB_Run method to run a inputed SQL Statment 
        ResultSet rs = DB_Run(SQL);
        Component frame = null;        
        JOptionPane.showMessageDialog(frame,"User User: "+username+" has been Deleted");
        System.out.println("User deleted");
        
        //Return single string
         return null;
    }
    
    public String DB_CreateMessage(String receiverID, String subject, String body) throws SQLException{
       
        //Create output variable 
        String output=null;
        
        int id = newID();
        
        //SQL String
        String sql = "INSERT INTO PRDCC.MESSAGES (ID, SENDER, RECEIVER, SUBJECT, BODY, READ) VALUES (" ; 
        sql += Integer.toString(id) + ", '";
        sql += "ADMIN" + "', '";
        sql += receiverID + "', '";
        sql += subject + "', '";
        sql +=  body + "', '";
        sql += "false" + "')";

        //Use the DB_Run method to run a inputed SQL Statment 
        try{
            ResultSet rs = DB_Run(sql);
            Component frame = null;
            JOptionPane.showMessageDialog(frame,"The message has been sent to "+receiverID+"");
            System.out.println("Message Sent");
        }
        catch(SQLException e){
        System.out.println(e.getMessage());
        }
        
        //Return single string
         return null;
    }
    
    private int newID() throws SQLException{
        int id = 0;
        String sql = "SELECT ID FROM (SELECT ID, RANK() OVER (ORDER BY ID DESC) ID_RANK FROM PRDCC.MESSAGES) WHERE ID_RANK <=1";
        ResultSet rs = DB_Run(sql);
        try{
            while(rs.next()){
                id = Integer.parseInt(rs.getString(1));
                id ++;
                return id;
            }
            System.out.println("New ID");
        } catch (SQLException e){
        
        }
        return id;
    }
    
     public String DB_DeleteRequest(String ID) throws SQLException{
       
        //Create output variable 
        String output=null;
        
        //SQL String
        String SQL="DELETE FROM REQUESTS WHERE ID ="+ID;

        //Use the DB_Run method to run a inputed SQL Statment 
        try{ResultSet rs = DB_Run(SQL);
        Component frame = null;        
        JOptionPane.showMessageDialog(frame,"Request: "+ID+" has been Deleted");
        System.out.println("Request deleted");}
        catch(SQLException e){
            System.out.println("SQL FAIL");
            System.out.println(e.getMessage());
        }
        
        //Return single string
         return null;
    }
     
     public String DB_DeleteAdvert(String ID) throws SQLException{
       
        //Create output variable 
        String output=null;
        
        //SQL String
        String SQL="DELETE FROM AD WHERE ID ="+ID;

        //Use the DB_Run method to run a inputed SQL Statment 
        try{ResultSet rs = DB_Run(SQL);
        Component frame = null;        
        JOptionPane.showMessageDialog(frame,"AD: "+ID+" has been Deleted");
        System.out.println("AD deleted");}
        catch(SQLException e){
            System.out.println("SQL FAIL");
            System.out.println(e.getMessage());
        }
        
        //Return single string
         return null;
    }
    
    public static void main(String[] args) throws SQLException {
       
         
    }
    
    
}

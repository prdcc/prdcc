/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prdcc.desktop.app.Model;

/**
 *
 * @author Josh
 */
public class Users {
    
    String username;
    String firstname;
    String surname;
    String email;
    String city;
    String postcode;
    String contactNumber;
    String uni;
    String password;
    int swaps;
    String status;

    public Users(String username, String firstname, String surname, String email, String city, String postcode, String contactNumber, String uni, String password, int swaps, String status) {
        this.username = username;
        this.firstname = firstname;
        this.surname = surname;
        this.email = email;
        this.city = city;
        this.postcode = postcode;
        this.contactNumber = contactNumber;
        this.uni = uni;
        this.password = password;
        this.swaps = swaps;
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getUni() {
        return uni;
    }

    public String getPassword() {
        return password;
    }

    public int getSwaps() {
        return swaps;
    }
    
    public String getStatus() {
        return status;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setUni(String uni) {
        this.uni = uni;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSwaps(int swaps) {
        this.swaps = swaps;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prdcc.desktop.app.Model;
import java.util.regex.Pattern;

/**
 *
 * @author Josh
 */
public class Validation {
    
    String input;
    
    
    
    public boolean validUser(String username, String firstname, String surname, String email, String city, String postcode, String contactNumber, String uni, String password, String swaps){
        boolean result = false; 
        
        if(isNotNull(username)&&isNotNull(firstname)&&isNotNull(surname)&&isNotNull(email)&&isNotNull(city)&&isNotNull(postcode)&&isDigit(contactNumber)&&isNotNull(uni)&&isNotNull(password)&&isDigit(swaps)){
            result = true;
        }
        
        return  result; 
    }
    
    public boolean validAdmin(String username, String date, String level, String password, String comments){
        boolean result = false; 
        
        if(isNotNull(username)&&isDate(date)&&isDigit(level)&&isNotNull(password)&&isNotNull(comments)){
            result = true;
        }
        
        return  result; 
    }
    
   
    public boolean isNotNull(String input){
        return Pattern.matches(".+", input);
    }
    
    public boolean isDigit(String input){
        return Pattern.matches("^[0-9]+$", input);
    }
    
    public boolean isDate(String input){
        //return Pattern.matches("/(0[1-9]|1[012])[- \\/.](0[1-9]|[12][0-9]|3[01])[- \\/.](19|20)\\d\\d/", input);
        return true;
    }
    
    
    
    
    
    
}

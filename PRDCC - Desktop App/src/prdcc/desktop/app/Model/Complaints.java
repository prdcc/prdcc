/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prdcc.desktop.app.Model;

/**
 *
 * @author Josh
 */
public class Complaints {
    
    int ID;
    String dateSent;
    String status;
    String user;
    int ad;
    int requests;
    String notes;

    public Complaints(int ID, String dateSent, String status, String user, int ad, int requests, String notes) {
        this.ID = ID;
        this.dateSent = dateSent;
        this.status = status;
        this.user = user;
        this.ad = ad;
        this.requests = requests;
        this.notes = notes;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDateSent() {
        return dateSent;
    }

    public void setDateSent(String dateSent) {
        this.dateSent = dateSent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getAd() {
        return ad;
    }

    public void setAd(int ad) {
        this.ad = ad;
    }

    public int getRequests() {
        return requests;
    }

    public void setRequests(int requests) {
        this.requests = requests;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    
    
    
}

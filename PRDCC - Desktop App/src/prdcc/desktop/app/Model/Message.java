/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prdcc.desktop.app.Model;

/**
 *
 * @author Josh
 */
public class Message {
    public int id;
    public String senderId;
    public String receiverId;
    public String subject;
    public String body;
    public String read;
    
    public Message(int id, String senderId, String receiverId, String subject, String body, String read){
            this.id = id;
            this.senderId = senderId;
            this.receiverId = receiverId;
            this.subject = subject;
            this.body = body;
            this.read = read;
    }
    
    

    public int getId() {
        return id;
    }

    public String getSenderId() {
        return senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public String getRead() {
        return read;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setRead(String read) {
        this.read = read;
    }
    
}

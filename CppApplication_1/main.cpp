#include <stdio.h>
#include <stdbool.h>
#include<stdlib.h>
#include<time.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>


int main(void) {
    
    bool num =false;
    bool Match = false;
    
    char pins[2];
    
    int posCount =8;
    int colsCount = 6;
    int black = 0;
    int white = 0;
    
    
    struct node {
        int values[8 + 2];
        struct node *next;
    };
    typedef struct node item;
    
    item * curr, * head;
    head = NULL;
    //int pattern[posCount];
    int attempts[posCount];
    
    
    
    //*********************************************** CREATE PATTERN *****************************************************
    int pattern[posCount];
    srand ( time(NULL) );
    
    for (int i = 0; i < posCount; i++){
        pattern[i] = rand() % colsCount + 1;
        printf("%d\t", pattern[i]);
    }
    //*******************************************************************************************************************
    
    printf("\nComputer has generated a pattern, time to start guessing!\n");
    
    while (!Match){
        //**********************************************FILL ATTEMPT*********************************************************
        black = 0;
        white =0;
        bool num;
        printf("\n");
        //take in a value for each position of the pattern
        int i = 0;
        while (i < posCount)
        {
            
                read(STDIN_FILENO, &attempts[i],1);
                
                i++;
                //scanf("%d", &attempts[i]);
              
        }
       
        //********************************************************************************************************************
        //********************************************** CALCULATE PINS ******************************************************
        white = 0;
        black = 0;
        for (int i = 0; i < posCount; i++)
        {
            //if attempt value is correct colour and correct position - add 1 black pin
            if (attempts[i] == pattern[i])
            {
                black++;
            }
            else
            {
                
                //if value isn't in the correct position, check if it is anyhwere in the pattern
                for (int j = 0; j < posCount; j++)
                {
                    //if attempt value is anywhere in pattern EXCEPT the same position - add 1 white pin
                    if ((attempts[i] == pattern[j]) && (attempts[i] != pattern[i]))
                    {
                        white++;
                    }
                    
                }
            }
            
        }
        pins[0] = black;
        pins[1] = white;
        //write(STDOUT_FILENO, pins[0], 1);
        //write(STDOUT_FILENO, pins[1], 1);
        //********************************************************************************************************************
        //************************************************ ADD HSITORY *******************************************************
        curr = (item *)malloc(sizeof(item));
        for (int i = 0; i < sizeof(curr->values)/sizeof(*curr->values); i++){
            curr->values[i] = attempts[i];
        }
        curr->values[posCount ] = pins[0];
        curr->values[posCount + 1] = pins[1];
        
        curr->next  = head;
        head = curr;
        
        //********************************************************************************************************************
        //************************************************ SHOW HISTORY ******************************************************
        curr = head;
        
        printf("History: \n");
        if (curr != 0){
            while (curr){
                for (int i = 0; i < sizeof(curr->values)/sizeof(*curr->values)-2; i++)
                {
                    printf("%d \t", curr->values[i]);
                }
                printf("\t Black : %d \t", curr->values[posCount ]);
                printf("White : %d\n", curr->values[posCount +1]);
                curr = curr->next;
                
            }
            
            
            
            
        }
        //********************************************************************************************************************
        //**************************************************** CHECK ATTEMPT *************************************************
        if (pins[0] == posCount)
        {
            Match = true;
        }
    }
    
    if (Match == true)
    {
        printf("Congratulations; you cracked the code!\n");
        for (int i = 0; i < posCount; i++)
        {
            printf("%d\t", pattern[i]);
        }
        printf("\n");
        
    }
    
    return 0;
}



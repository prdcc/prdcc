<!DOCTYPE html>
<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>PlymLETS: Your Account</title>

    <!-- Bootstrap core CSS -->
	<!--<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="css\bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
			<a class="navbar-brand" rel="home" href="/" title="Plymouth LETS"><h3 style="color:white;">Plymouth LETS</h3></a>
    </div>

    <div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
					<li><a href="postAdd.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post an Ad</h4></a></li>
					<li><a href="postRequest.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post a Request</h4></a></li>
			</ul>
			<div class="col-sm-3" style="margin-left:200px;">
				<form class="navbar-form" method= "post" style="width:300px; margin-top:25px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>			
					</div>			
					<!--<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Category/subCategory...
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
						</ul>
					</div>-->
			
				</form>
			</div>
			<ul class="nav navbar-nav" style="float:right;">
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="account.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Your Account</h4></a></li>
				<li><a href=""><h4 style="font-weight:bold; color:white; margin-top:25px;">Sign Out</h4></a></li>
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="register.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Register</h4></a> </li>
			</ul>
		</div>
    </nav>
	

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
	  <br />
	   <h1>Your Requests</h1><br />
		<p>This is your Requests page. From here you can view all the requests you have posted and their current status.</p>
		
</div>
    </div>
	
	<div class="col-sm-3 col-md-2 sidebar" style=" height: 300px; text-align: right;">
         
          <ul class="nav nav-sidebar" style="font-size:20px;">
			<li><a href="account.php" style="font-weight:bold;">Account</a></li>
            <li><a href="" style="font-weight:bold;">Posts</a></li>
			<ul class="nav nav-sidebar">
			<li style= "padding-right: 5%;"><a href="yourAds.php">Ads</a></li>
            <li style= "padding-right: 5%;"><a href="yourRequests.php">Requests</a></li>
			
            <li><a href="swapHistory.php" style="font-weight:bold;">Swap History</a></li>
            <li style= "padding-right: 5%;"><a href="leaveFeedback.php">Leave Feedback</a></li>
            
          </ul>
     </div>
	<div class="container">
    
   
    
    <!-- Tab panes -->
    
		
         <div class="row">
		 
		<div class='col-md-4'>
					<h2>Dog Walking</h2><img class='img-circle' src='https://d13yacurqjgara.cloudfront.net/users/1960/screenshots/309611/doggy.jpg' alt='Generic placeholder image' style='width: 140px; height: 140px;'>
					<p>I'm offering a dog walking service to any dog-owning maths teachers who would be willing to provide tutoring for my maths GCSE.</p>
					<p style="color: red;">PENDING</p>
					<p><a class='btn btn-default' href='http://getbootstrap.com/examples/jumbotron/#' role='button'>View details »</a></p>
				</div>
				<div class='col-md-4'>
					<h2>Baby-gro's 18mths</h2><img class='img-circle' src='http://www.tractorted.co.uk/prodimg/BABYGROWFM_1_Zoom.jpg' alt='Generic placeholder image' style='width: 140px; height: 140px;'>
					<p>I'm getting rid of my baby grows (aged 18mths) as my little one has grown too big for them!.</p>
					<p style="color: red;">PENDING</p>
					<p><a class='btn btn-default' href='http://getbootstrap.com/examples/jumbotron/#' role='button'>View details »</a></p>
				</div>
				<div class='col-md-4'>
					<h2>Web Designer</h2><img class='img-circle' src='http://www.webdesignhighwycombe.com/images/internet-marketing-services.jpg' alt='Generic placeholder image' style='width: 140px; height: 140px;'>
					<p>I'm looking to set up a small website for my photography business. I need a small site to display my photos and allow people to contact me.</p>
					<p style="color: green;">LIVE</p>
					<p><a class='btn btn-default' href='http://getbootstrap.com/examples/jumbotron/#' role='button'>View details »</a></p>
				</div>
				<div class='col-md-4'>
					<h2>Maths Tutoring</h2><img class='img-circle' src='http://cliparts.co/cliparts/6cp/Lrg/6cpLrgyEi.jpg' alt='Generic placeholder image' style='width: 140px; height: 140px;'>
					<p>I'm an A-level maths teacher at fakeSchool, offering maths tutoring at all level up to and incluing key stage 5.</p>
					<p style="color: red;">PENDING</p>
					<p><a class='btn btn-default' href='viewAdd.php' role='button'>View details »</a></p>
				</div>
			
        
      </div>
  
    
</div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./index_files/jquery.min.js"></script>
    <script src="./index_files/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./index_files/ie10-viewport-bug-workaround.js"></script>
  

</body></html>
<?php 
		ob_start();
			if(isset($_POST['price']))
			{
				$price = $_POST['price'];
		
				if ($price > 8){
					echo "";
				}
				else
				{
					session_start();
					$_SESSION['purch'] = true;
					
					header("Location: swapHistory.php?swap=true"); /* Redirect browser */
					exit();
				}
			}
			
		?>
<!DOCTYPE html>
<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>Plymouth LETS</title>

    <!-- Bootstrap core CSS -->
	<!--<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="css\bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" rel="home" href="/" title="Plymouth LETS"><h3 style="color:white;">Plymouth LETS</h3></a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a href="postAdd.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post an Ad</h4></a></li>
				<li><a href="postRequest.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post a Request</h4></a></li>
			</ul>
			<div class="col-sm-3" style="margin-left:200px;">
				<form class="navbar-form" method= "post" style="width:300px; margin-top:25px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>			
					</div>			
					<!--<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Category/subCategory...
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
								<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
								<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
						</ul>
					</div>-->
			
				</form>
			</div>
			<ul class="nav navbar-nav" style="float:right;">
					<li><a href=""> </a></li>
					<li><a href=""></a></li>
					<li><a href=""> </a></li>
					<li><a href="account.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Your Account</h4></a></li>
					<li><a href=""><h4 style="font-weight:bold; color:white; margin-top:25px;">Sign Out</h4></a></li>
					<li><a href=""> </a></li>
					<li><a href=""></a></li>
					<li><a href=""> </a></li>
					<li><a href="register.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Register</h4></a> </li>
				</ul>
		</div>		
    </nav>
	

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
		<div class="container" style="height:200px; margin-top:-30px;">
			<br />
			<h1><img class='img-circle' src='http://cliparts.co/cliparts/6cp/Lrg/6cpLrgyEi.jpg' alt='Generic placeholder image' style='width: 140px; height: 140px;'> Maths Tutoring </h1><br />
			<p>I'm an A-level maths teacher at fakeSchool, offering maths tutoring at all level up to and incluing key stage 5.</p>
		</div>
    </div>
	<div class="container">
    <!-- Tab panes -->
		<div class="row">
			<div class='col-md-4' >
				<h3 style= "text-align: center;"><a href = ""> funkyTeacher</a>'s Profile</h3><br />
				<p  style = "text-align: center; font-size:20px;">Feedback Score:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;">4/5</p><br />
				<p  style = "text-align: center; font-size:20px;">Member Since:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;">2014</p><br />
				<p  style = "text-align: center; font-size:20px;">Swaps Completed:</p>
				<p  style = "text-align: center; font-weight: bold;">12</p><br />
			</div>
			<div class='col-md-4' >
				<h3 style= "text-align: center;">Location Details</h3><br />
				<p  style = "text-align: center; font-size:20px;">City:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;">Plymouth</p><br />
				<p  style = "text-align: center; font-size:20px;">Postcode:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;">PL4 6HJ</p><br />
				<p  style = "text-align: center; font-size:20px;">Available within:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;">2miles</p><br />
			</div>
			<div class='col-md-4' >
				<h3 style= "text-align: center;">Listing Details</h3><br />
				<p  style = "text-align: center; font-size:20px;">Price:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;">7swaps p/h</p><br />
				<p  style = "text-align: center; font-size:20px;">Ending Date:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;">01/03/2015</p><br />
				<p  style = "text-align: center; font-size:20px;">Would swap for:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;">Web design</p><br />
			</div>
			
			<div class='col-md-4'style = "width: 100%; text-align: center; margin-top: 2%;  ">
				<p style= "text-align: center; font-weight:bold; color:red; font-size:50px;">Purchase Below!</p><br />
				<form action: = "" method="post">
					<p style="text-align: left; font-size:25px;"> Your current swap-balance is: 8</p>
					<p style="font-size:18px;">Swaps: 
						<select name="price" style="width: 25%">
							<option value = "hi"> Price (p/h)... </option>
							<option value = "7">7 (1 hour)</option>
							<option value = "14">14 (2 hours)</option>
							<option value = "21">21 (3 hours)</option>
						</select>
						<button type="button" class="btn btn-default" role="button" style="margin-right: 10%;" data-toggle="modal" data-target ="#confirm" style="font-size:18px;">Swap!</button>
						<div class = "modal" id="confirm">
							<div class = "modal-dialog">
								<div class = "modal-content">
									<div class = "modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h3>Confirm swap</h3>
									</div>
									<div class = "modal-body">
									Do you want to swap for Maths Tutoring?
									</div>
									<div class ="modal-footer">
										<button type="submit" class="btn btn-default" role="button" data-toggle="modal" data-target ="#confirm">Swap!</button>
										<a class ="btn btn-default">No</a>
									</div>
								</div>
							</div>
						</div>
					</p>
					<p style="font-size:18px; margin-right:75px;">Offer:
						<select name="offers" style="width: 25%">
							<option value = "hi"> Your ads... </option>
							<option value = "hi">Ad1</option>
							<option value = "hi"> Ad2</option>
						</select>
						<a class="btn btn-default" href="viewAdd.php" role="button"  style="font-size:18x;">Make Offer »</a>
					</p>			
				</form>
			</div>        
		</div>    
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./index_files/jquery.min.js"></script>
    <script src="./index_files/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./index_files/ie10-viewport-bug-workaround.js"></script>
  

</body></html>

<%@page import="message.Message"%>
<!DOCTYPE html>

<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<%@page import="java.util.ArrayList"%>
<%@page import = "java.sql.*" %>
<%@page import = "java.sql.Connection" %>
<%@page import = "java.sql.DriverManager" %>
<%@page import = "java.sql.SQLException" %>

<%@page import = "ad.Ad" %>
<%@page import = "request.Request"%>
<%@page import = "database.Database2" %>



<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="http://getbootstrap.com/favicon.ico">

        <title>Plymouth LETS</title>

        Bootstrap core CSS 
        <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="css\bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>


    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" rel="home" href="index.jsp" title="Plymouth LETS"><img src="logo-new.png"></a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
					<li><a href="postAdd.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post an Ad</h4></a></li>
					<li><a href="postRequest.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post a Request</h4></a></li>
			</ul>
			<div class="col-sm-3" style="margin-left:200px;">
				<form action = "index.jsp" class="navbar-form" method="post" style="width:300px; margin-top:25px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="searchKey" id="searchKey">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>
					</div>	
                                </form>
					<!--<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Category/subCategory...
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
						</ul>
					</div>-->
			
				
			</div>
			<ul class="nav navbar-nav" style="float:right;">
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="account.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Your Account</h4></a></li>
				
                                            <%  if (session.getAttribute("logged_in") == null || (String) session.getAttribute("logged_in") == "false")
                                            {
                                                out.println("<li><a href='signIn.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign IN");
                                            }
                                            else if ((String) session.getAttribute("logged_in") == "true") {
                                                out.println("<li><a href='signOut.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign OUT, " + session.getAttribute("username"));
                                            }
                                            
                                            %></h4></a></li>
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="register.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Register</h4></a> </li>
			</ul>
		</div>	
    </nav>


        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
                <h1 style="font-weight:bold;">Your Messages</h1>
                <p style="font-size:18px;">This is your message box. From here you can view your sent and received messages, and reply to them.</p>
                <br>
            </div>
        </div>




        <div class="container">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active">
                    <a href="#inbox" role="tab" data-toggle="tab" style="font-size:28px;">
                        <span class="badge" style = "font-size:22px;"><%out.println(Message.countUnread((String) session.getAttribute("username")));%></span><i class="fa fa-user"></i> Inbox
                    </a>
                </li>
                <li><a href="#Sent" role="tab" data-toggle="tab" style="font-size:28px;">
                        <i class="fa fa-user"></i> Sent
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane fade active in" id="inbox">
                    <div class="row">
                        <%
                            //Database2 database = new Database2();
                            ArrayList<Message> msgs = null;
                            //database.query(database.prepare("select * from PRDCC.AD"));

                            msgs = Message.findBySQL("select * from PRDCC.MESSAGES WHERE RECEIVER = '" + session.getAttribute("username") + "' ORDER BY READ ASC");

                            for (Message msg : msgs) {
                                out.println("<div class='col-md-12'>"
                                        + "<h2>" + msg.getSubject() + "</h2>"
                                        + "<p>" + msg.getBody() + "</p>");
                                //+ "<p><a class='btn btn-default' href='viewAdd.jsp?id="+ad.id+"' role='button'>View details �</a></p>"
                                if (msg.getRead().equals("false")) {
                                    out.println("<p><a class='btn btn-default' href='markAsRead.jsp?msgToRead=" + msg.getId() + "&action=read' role='button'>Mark as read </a>");
                                } else if (msg.getRead().equals("true")) {
                                    out.println("<p><a class='btn btn-default' href='markAsRead.jsp?msgToRead=" + msg.getId() + "&action=unread' role='button'>Mark as Unread </a>");
                                }
                                out.println("<a class='btn btn-default' href='deleteMsg.jsp?id=" + msg.getId() + "' role='button'>Delete message </a>"
                                        + "<a class='btn btn-default' href='msg.jsp?re=" + msg.getSenderId() + "' role='button'>Reply �</a></p>"
                                        + "</div><br />");

                            }

                        %>



                    </div>
                </div>
                <div class="tab-pane fade" id="Sent">
                    <div class="row">
                        <%                  ArrayList<Message> sent = null;
                            sent = Message.findBySQL("select * from PRDCC.MESSAGES WHERE SENDER = '" + session.getAttribute("username") + "' ORDER BY READ ASC");

                            for (Message msg : sent) {
                                out.println("<div class='col-md-12'>"
                                        + "<h2>" + msg.getSubject() + "</h2>"
                                        + "<p>" + msg.getBody() + "</p>");
                                //+ "<p><a class='btn btn-default' href='viewAdd.jsp?id="+ad.id+"' role='button'>View details �</a></p>"

                                out.println("<a class='btn btn-default' href='deleteMsg.jsp?id=" + msg.getId() + "' role='button'>Delete message </a>"
                                        + "</div><br />");
                            }
                        %>
                    </div>
                </div>

            </div>

        </div>



        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="./index_files/jquery.min.js"></script>
        <script src="./index_files/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="./index_files/ie10-viewport-bug-workaround.js"></script>


    </body></html>
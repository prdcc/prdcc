<%@page import="java.util.ArrayList"%>
<%@page import = "java.sql.*" %>
<%@page import = "java.sql.Connection" %>
<%@page import = "java.sql.DriverManager" %>
<%@page import = "java.sql.SQLException" %>

<%@page import = "request.Request"%>
<%@page import = "database.Database2" %>
<%@page import = "category.Category" %>
<!DOCTYPE html>
<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<%
            Category products = (Category) Category.findBySQL("SELECT * FROM PRDCC.CATEGORYS WHERE CATEGORYS = 'Product'").get(0);
    Category services = (Category) Category.findBySQL("SELECT * FROM PRDCC.CATEGORYS WHERE CATEGORYS = 'Service'").get(0);
    
    if ((session.getAttribute("logged_in") == null || session.getAttribute("logged_in") == "false") || session.getAttribute("username") == null) {
        String site = "signIn.jsp";
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);
    } else if (request.getParameter("title") != null  && request.getParameter("description") != null && request.getParameter("city") != null && request.getParameter("postcode") != null && request.getParameter("distance") != null
            && request.getParameter("price") != null && request.getParameter("needby_date") != null) {

        String Cat;    
        if(products.getSubCats().contains(request.getParameter("subcat"))){
                Cat = "Product";
            }
            else{
                Cat = "Service";
            }
        
        Request newReq = new Request(0, request.getParameter("title"), Integer.parseInt(request.getParameter("price")), request.getParameter("description"), Cat, request.getParameter("subcat"), (String) session.getAttribute("username"), "PENDING", "01/01/2001", request.getParameter("needby_date"), request.getParameter("city"), request.getParameter("postcode"), Integer.parseInt(request.getParameter("distance")), request.getParameter("swap_for"));
        newReq.create();

        String site = "yourRequests.jsp";
        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", site);
    }

        //out.println(res.getString(0));

%>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="http://getbootstrap.com/favicon.ico">

        <title>PlymLETS: Post Request</title>

        <!-- Bootstrap core CSS -->
        <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="css\bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>
        
        <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.2.min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>


    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" rel="home" href="index.jsp" title="Plymouth LETS"><img src="logo-new.png"></a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
					<li><a href="postAdd.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post an Ad</h4></a></li>
					<li><a href="postRequest.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post a Request</h4></a></li>
			</ul>
			<div class="col-sm-3" style="margin-left:200px;">
				<form action = "index.jsp" class="navbar-form" method="post" style="width:300px; margin-top:25px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="searchKey" id="searchKey">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>
					</div>	
                                </form>
					<!--<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Category/subCategory...
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
						</ul>
					</div>-->
			
				
			</div>
			<ul class="nav navbar-nav" style="float:right;">
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="account.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Your Account</h4></a></li>
				
                                            <%  if (session.getAttribute("logged_in") == null || (String) session.getAttribute("logged_in") == "false")
                                            {
                                                out.println("<li><a href='signIn.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign IN");
                                            }
                                            else if ((String) session.getAttribute("logged_in") == "true") {
                                                out.println("<li><a href='signOut.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign OUT, " + session.getAttribute("username"));
                                            }
                                            
                                            %></h4></a></li>
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="register.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Register</h4></a> </li>
			</ul>
		</div>	
    </nav>


        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
                <br />
                <h1>Post a Request</h1><br />
                <p>A Request is for a product or service that you require. Users can respond to a request by suggesting a service they offer that matches your requests criteria.

                    Provide the details of your request below.</p>
            </div>
        </div>
                <form method="post">
                    <div class ="container">
                    <div class="input-group" style = "text-align: center;">
                        <input type="text" class="form-control" required = "required" maxlength = "50" pattern = "^[a-zA-Z0-9_ ]*$" title = "Only alphanumeric characters"placeholder="Title" name="title" id="title" style = "width: 140%; font-size:16px;"><p style = "color: rgb(238, 238, 238);">spacing</p>
                    </div>
                <div class="input-group">                                            
                    <input type="text" readonly = "readonly" placeholder = "Category" required = "required"  name ="subcat" ID="subcat" Class="form-control"></input>
                    <div class="input-group-btn">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul id="demolist" class="dropdown-menu">
                            <li role="presentation" class="dropdown-header">Product</li>
                            <%for (String sub : products.getSubCats()) {
                    out.println("<li><a href='#'>" + sub + "</a></li>");
                }%>


                            <li role="presentation" class="divider"></li>  
                            <li role="presentation" class="dropdown-header">Service</li>

                            <%for (String sub2 : services.getSubCats()) {
                                    out.println("<li role='presentation'><a role='menuitem' tabindex='-1' href='#'>" + sub2 + "</a></li>");
                                }%>
                        </ul>
                    </div>
                </div>
                <script> $(document).on('click', '.dropdown-menu li a', function() {
                            $('#subcat').val($(this).html());
                            
                        }); 
                </script>
                    
                    
                <p style = "color: rgb(238, 238, 238);">spacing</p>                        
                    <div class="form-group">
                        <textarea class="form-control" required = "required" maxlength = "2000"  placeholder="Description..."rows="5" id="description" name="description" style="font-size:16px;"></textarea>
                    </div>
                    </div>
                




        <div class="container">

            <div class="row">
                <div class='col-md-4' >
                    <h3 style= "text-align: center;">Your Profile</h3><br />

                    <p  style = "text-align: center; font-size:20px;">Feedback Score:</p>
                    <p  style = "text-align: center; font-weight: bold; font-size:16px;">4/5</p><br />
                    <p  style = "text-align: center; font-size:20px;">Member Since:</p>
                    <p  style = "text-align: center; font-weight: bold; font-size:16px;">2014</p><br />
                    <p  style = "text-align: center; font-size:20px;">Swaps Completed:</p>
                    <p  style = "text-align: center; font-weight: bold; font-size:16px;">12</p><br />
                </div>
                <div class='col-md-4' >
                    <h3 >Location Details</h3><br />

                    
                        <div class="input-group" style = "text-align: center;">
                            <input type="text" class="form-control" required = "required" maxlength = "30" pattern = "^[a-zA-Z]+$" title = "Only alphabetical characters" placeholder="City" name="city" id="city" style = "width: 140%; margin: 0 5% 0 0; font-size:16px;"><p style = "color: white;">spacing</p>
                            <input type="text" class="form-control" required = "required" maxlength = "3" pattern = "^[0-9]+$" title = "Only numerical characters" placeholder="Distance willing to travel (miles)" name="distance" id="distance"style = "width: 140%; margin: 0 5% 0 0; font-size:16px;"><p style = "color: white;">spacing</p>
                            <input type="text" class="form-control" placeholder="Your postcode" required = "required" maxlength = "8" pattern = "PL[0-9]{1,2}\s?[0-9]{1,1}[A-Z]{2,2}$|pl[0-9]{1,2}\s?[0-9]{1,1}[a-z]{2,2}$" title = "Only Plymouth postcodes" name="postcode" id="postcode"style = "width: 140%; margin: 0 5% 0 0; font-size:16px;"><p style = "color: white;">spacing</p>
                        </div>
                    
                </div>
                <div class='col-md-4' >
                    <h3 >Listing Details</h3><br />

                    
                        <div class="input-group" style = "text-align: center;">
                            <input type="text" class="form-control" required = "required" maxlength = "3" pattern = "^[0-9]+$" title =  "Only numerical characters" placeholder="Willing to pay (swaps)" name="price" id="price" style = "width: 140%; margin: 0 5% 0 0; font-size:16px;"><p style = "color: white;">spacing</p>
                            <input type="text" class="form-control" required="required" placeholder="Needed by (mm/dd/yyyy)" name="needby_date" id="needby_date"style = "width: 140%; margin: 0 5% 0 0; font-size:16px;"><p style = "color: white;">spacing</p>
                            <input type="text" class="form-control" placeholder="I can swap for..." name="swap_for" id="swap_for"style = "width: 140%; margin: 0 5% 0 0; font-size:16px;"><p style = "color: white;">spacing</p>
                        </div>
                        <button class="btn-lg btn-success" type="submit" href="http://example.com">Post Request!</button>
                        <button class="btn-lg btn-default" type="button" href="http://example.com">Cancel</button>
                    
                </div>

                <!--<div class='col-md-4'style = "width: 100%; text-align: center; margin-top: 2%;">
                        <h3 style= "text-align: center;">Viewers options</h3><br />
                        <form action: = "" method="post">
                        
                        Offer:
                        <select name="offers" style="width: 25%">
                                <option value = "hi"> Your ads... </option>
                                <option value = "hi">Ad1</option>
                                <option value = "hi"> Ad2</option>
                </select>
                        <a class="btn btn-default" href="http://getbootstrap.com/examples/jumbotron/#" role="button">Make Offer »</a></p><br />
                        
                </form>

                </div>-->


            </div>


        </div>
                </form>
                



        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="./index_files/jquery.min.js"></script>
        <script src="./index_files/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="./index_files/ie10-viewport-bug-workaround.js"></script>


    </body></html>
<?php
 session_start();

?>
<!DOCTYPE html>
<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>Plymouth LETS</title>

    <!-- Bootstrap core CSS -->
	<!--<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="css\bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" rel="home" href="/" title="Plymouth LETS">Plymouth LETS</a>
    </div>

    <div class="collapse navbar-collapse navbar-ex1-collapse">

        <ul class="nav navbar-nav">
            <li><a href="">Post an Ad</a></li>
            <li><a href="">Post a Request</a></li>
            <li><a href=""> </a></li>
			<li><a href=""></a></li>
			<li><a href=""> </a></li>
            <li><a href="">Your Account</a></li>
			<li><a href="">Sign Out</a></li>
        </ul>

        <div class="col-sm-3 col-md-3 pull-right">
        <form class="navbar-form" method= "post">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
			
        </div>
		
		<div class="dropdown">
			<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
				Category/subCategory...
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
					<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
					<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
					
			</ul>
		</div>
		
        </form>
        </div>

    </div>
	
    </nav>
	

    <!-- Main jumbotron for a primary marketing message or call to action -->
    
   <div class='jumbotron'>
				<div class='container'>
					<br />
					<h1><img class='img-circle' src='data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==' alt='Generic placeholder image' style='width: 140px; height: 140px;'> <?php echo $_SESSION['title'];?> </h1><br />
					<p><?php echo $_SESSION['description'];?></p>
				</div>
			</div>
	

				<div class='container'>
    
   
    
				<!-- Tab panes -->
    
      
				<div class='row'>
					<div class='col-md-4' >
						<h3 style= 'text-align: center;'><a href = ""> jSmith</a>'s Profile</h3><br />
					
						<p  style= 'text-align: center;'>Feedback Score:</p>
						<p  style = 'text-align: center; font-weight: bold;'>5/5</p><br />
						<p  style= 'text-align: center;'>Member Since:</p>
						<p  style = 'text-align: center; font-weight: bold;'>2015</p><br />
						<p  style= 'text-align: center;'>Swaps Completed:</p>
						<p  style = 'text-align: center; font-weight: bold;'>1</p><br />
					</div>
					<div class='col-md-4' >
						<h3 style= 'text-align: center;'>Location Details</h3><br />
					
						<p  style= 'text-align: center;'>City:</p>
						<p  style = 'text-align: center; font-weight: bold;'><?php echo $_SESSION['city'];?></p><br />
						<p  style= 'text-align: center;'>Postcode:</p>
						<p  style = 'text-align: center; font-weight: bold;'><?php echo $_SESSION['postcode'];?></p><br />
						<p  style= 'text-align: center;'>Available within:</p>
						<p  style = 'text-align: center; font-weight: bold;'><?php echo $_SESSION['distance'];?></p><br />
					</div>
					<div class='col-md-4' >
						<h3 style= 'text-align: center;'>Listing Details</h3><br />
					
						<p  style= 'text-align: center;'>Price:</p>
						<p  style = 'text-align: center; font-weight: bold;'><?php echo $_SESSION['price'];?></p><br />
						<p  style= 'text-align: center;'>Ending Date:</p>
						<p  style = 'text-align: center; font-weight: bold;'><?php echo $_SESSION['end_date'];?></p><br />
						<p  style= 'text-align: center;'>Would swap for:</p>
						<p  style = 'text-align: center; font-weight: bold;'><?php echo $_SESSION['swap_for'];?></p><br />
					</div>
			
					<div class='col-md-4'style = 'width: 100%; text-align: center; margin-top: 2%;  '>
						<h3 style= 'text-align: center;'>I want this!</h3><br />
						<form action: = '' method='post'>
						<p style='text-align: left;'> Your current swap-balance is: 8</p>
		
						<p>Swaps: 
						<select name='price' style='width: 25%'>
							<option value = 'hi'> Price... </option>
							<option value = '7'>7</option>
							<option value = '14'>14</option>
							<option value = '21'>21</option>
						</select>
		
						<button type='button' class='btn btn-default' role='button' style='margin-right: 10%;' data-toggle='modal' data-target ='#confirm'>Swap!</button>
						<div class = 'modal' id='confirm'>
							<div class = 'modal-dialog'>
								<div class = 'modal-content'>
									<div class = 'modal-header'>
										<button type='button' class='close' data-dismiss='modal'>&times;</button>
										<h3>Confirm swap</h3>
									</div>
								<div class = 'modal-body'>
										Do you want to swap for Maths Tutoring?
								</div>
								<div class ='modal-footer'>
									<button type='submit' class='btn btn-default' role='button' data-toggle='modal' data-target ='#confirm'>Swap!</button>
									<a class ='btn btn-default'>No</a>
								</div>
							</div>
						</div>
					</div>
		 
		
					Offer:
					<select name='offers' style='width: 25%'>
						<option value = 'hi'> Your ads... </option>
						<option value = 'hi'>Ad1</option>
						<option value = 'hi'> Ad2</option>
					</select>
					<a class='btn btn-default' href='http://getbootstrap.com/examples/jumbotron/#' role='button'>Make Offer »</a></p><br />
			
			</form>

		</div>
			
        
      </div>
  
    
</div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./index_files/jquery.min.js"></script>
    <script src="./index_files/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./index_files/ie10-viewport-bug-workaround.js"></script>
  

</body></html>
<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<%@page import="java.util.ArrayList"%>
<%@page import = "java.sql.*" %>
<%@page import = "java.sql.Connection" %>
<%@page import = "java.sql.DriverManager" %>
<%@page import = "java.sql.SQLException" %>

<%@page import = "ad.Ad" %>
<%@page import = "ad.Archive" %>
<%@page import = "user.User" %>
<%@page import = "feedback.Feedback"%>
<%@page import = "database.Database2" %>
<%
    String id = request.getParameter("id");
    User user = null;
    
    Ad advert = (Ad) Ad.findBySQL("SELECT * FROM PRDCC.AD WHERE ID ='"+id+"'", false).get(0);
    if(session.getAttribute("logged_in")!="false" && session.getAttribute("username") != null && session.getAttribute("username") != "null")
    {
       user = (User) User.findBySQL("SELECT * FROM PRDCC.USERS WHERE USERNAME = '" +session.getAttribute("username")+"'").get(0);
    }
    if (!advert.getStatus().equals("APPROVED")){
        if (user !=null){
            if(!user.getUsername().equals(advert.getSellerId())){
                String site = "index.jsp";
                response.setStatus(response.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", site);
            }
        }
        else{
            String site = "index.jsp";
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", site);
        }
    }
    User seller = (User) User.findBySQL("SELECT * FROM PRDCC.USERS WHERE USERNAME = '" +advert.getSellerId() + "'").get(0);
    int feedbackSeller, feedbackBuyer, swaps;
    feedbackSeller = Feedback.count("SELECT (SUM(RATING)/COUNT(ID)) FROM PRDCC.FEDBACK WHERE ARCHIVE IN (SELECT ID FROM PRDCC.ARCHIVE WHERE SELLER = '"+seller.getUsername()+"')");
    feedbackBuyer = Feedback.count("SELECT (SUM(RATING)/COUNT(ID)) FROM PRDCC.FEDBACK WHERE ARCHIVE IN (SELECT ID FROM PRDCC.ARCHIVE WHERE BUYER = '"+seller.getUsername()+"')");
    swaps = Archive.count("SELECT COUNT(ID) FROM PRDCC.ARCHIVE WHERE SELLER ='"+seller.getUsername()+"' OR BUYER = '"+seller.getUsername()+"'");

%>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>Plymouth LETS</title>

    <!-- Bootstrap core CSS -->
	<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css\bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" rel="home" href="index.jsp" title="Plymouth LETS"><img src="logo-new.png"></a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
					<li><a href="postAdd.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post an Ad</h4></a></li>
					<li><a href="postRequest.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post a Request</h4></a></li>
			</ul>
			<div class="col-sm-3" style="margin-left:200px;">
				<form action = "index.jsp" class="navbar-form" method="post" style="width:300px; margin-top:25px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="searchKey" id="searchKey">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>
					</div>	
                                </form>
					<!--<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Category/subCategory...
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
						</ul>
					</div>-->
			
				
			</div>
			<ul class="nav navbar-nav" style="float:right;">
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="account.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Your Account</h4></a></li>
				
                                            <%  if (session.getAttribute("logged_in") == null || (String) session.getAttribute("logged_in") == "false")
                                            {
                                                out.println("<li><a href='signIn.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign IN");
                                            }
                                            else if ((String) session.getAttribute("logged_in") == "true") {
                                                out.println("<li><a href='signOut.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign OUT, " + session.getAttribute("username"));
                                            }
                                            
                                            %></h4></a></li>
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="register.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Register</h4></a> </li>
			</ul>
		</div>	
    </nav>
	

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
		<div class="container" style="height:200px; margin-top:-30px;">
			<br />
			<h1><img class='img-circle' src='ad.png' alt='Generic placeholder image' style='width: 140px; height: 140px;'> <%out.println(advert.getTitle());%> </h1><br />
			<p><%out.println(advert.getDescription());%> </p>
		</div>
    </div>
	<div class="container">
    <!-- Tab panes -->
		<div class="row">
			<div class='col-md-4' >
                            <h3 style= "text-align: center;"><a href = "<%if (session.getAttribute("username")!= null){if (session.getAttribute("username").equals(advert.getSellerId())) {out.println("account.jsp");} else {out.println("msg.jsp?re=" +advert.getSellerId());}}%>"><%out.println(advert.getSellerId());%></a>'s Profile</h3><br />
				<p  style = "text-align: center; font-size:20px;">Feedback As a Seller:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;"><%out.println(Integer.toString(feedbackSeller)+"/5");%></p><br />
				<p  style = "text-align: center; font-size:20px;">Feedback As a Buyer:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;"><%out.println(Integer.toString(feedbackBuyer)+"/5");%></p><br />
				<p  style = "text-align: center; font-size:20px;">Swaps Completed:</p>
				<p  style = "text-align: center; font-weight: bold;"><%out.println(Integer.toString(swaps));%></p><br />
			</div>
			<div class='col-md-4' >
				<h3 style= "text-align: center;">Location Details</h3><br />
				<p  style = "text-align: center; font-size:20px;">City:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;"><%out.println(advert.getCity());%> </p><br />
				<p  style = "text-align: center; font-size:20px;">Postcode:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;"><%out.println(advert.getPostcode());%> </p><br />
				<p  style = "text-align: center; font-size:20px;">Available within:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;"><%out.println(advert.getDistance());%> miles</p><br />
			</div>
			<div class='col-md-4' >
				<h3 style= "text-align: center;">Listing Details</h3><br />
				<p  style = "text-align: center; font-size:20px;">Price:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;"><%out.println(advert.getPrice());%> swaps p/h</p><br />
				<p  style = "text-align: center; font-size:20px;">Ending Date:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;"><%out.println(advert.getEndDate());%> </p><br />
				<p  style = "text-align: center; font-size:20px;">Would swap for:</p>
				<p  style = "text-align: center; font-weight: bold; font-size:16px;">Web design</p><br />
			</div>
			
			<div class='col-md-4'style = "width: 100%; text-align: center; margin-top: 2%;  ">
				<p style= "text-align: center; font-weight:bold; color:red; font-size:50px;">Purchase Below!</p><br />
                                
                                <%
                                if (session.getAttribute("username")!=null && session.getAttribute("logged_in") == ("true")){
                                          
                                            
                                if (!advert.getSellerId().equals(session.getAttribute("username"))){
                                    out.println("<form action = 'purchaseAd.jsp?ad="+advert.getId()+"' method='post'>"
                                    +"<p style='text-align: left; font-size:25px;'> Your current swap-balance is: ");if (session.getAttribute("username")!=null && session.getAttribute("logged_in") == ("true")){ out.println(user.getBalance());}out.println("</p>"
                                    +"<p style='font-size:18px;'>Swaps:"
                                    +"<select name='price' style='width: 25%'>"
                                        +"<option value = '1'>"+advert.price + "swaps - 1hour</option>"
                                        +"<option value = '2'>" + advert.price *2 + "swaps - 2hours</option>"
                                       +" <option value = '3'>"+advert.price *3 + "swaps - 3hours</option>"
                                    +"</select>"
                                    +"<button class='btn-lg btn-success' type='submit'>Purchase</button>"
                                +"</form>");
                               
                                    ArrayList<Ad> viewersAds = Ad.findBySQL("SELECT * FROM PRDCC.AD WHERE SELLER = '" + (String) session.getAttribute("username") +"'", false);
                                            out.println("<form action = 'makeOffer.jsp?ad="+advert.getId()+"' method='post'>"
                                                            + "<p style='font-size:18px; margin-right:75px;'>Offer:"
                                                                +"<select name='offers'style='width: 25%'>");
                                                                    for (Ad ad : viewersAds){
                                                                        out.println("<option value = '"+ad.getId() + "'>"+ad.getTitle()+"</option>");
                                                                    }
                                                                out.println("</select>");
                                                                if (viewersAds.isEmpty()){
                                                                    out.println("<button class='btn-lg btn-success' disabled = 'disabled' type='submit'>Make offer</button>");
                                                                } else {
                                                                   out.println("<button class='btn-lg btn-success' type='submit'>Make offer</button>"); 
                                                                }
                                                            out.println("</p>"
                                                        +"</form>");
                                        
                                        }
                                }
				%>
				
			</div>        
		</div>    
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./index_files/jquery.min.js"></script>
    <script src="./index_files/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./index_files/ie10-viewport-bug-workaround.js"></script>
  

</body></html>
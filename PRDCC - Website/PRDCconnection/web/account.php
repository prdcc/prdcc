<!DOCTYPE html>
<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>PlymLETS: Your Account</title>

    <!-- Bootstrap core CSS -->
	<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css\bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
			<a class="navbar-brand" rel="home" href="/" title="Plymouth LETS"><h3 style="color:white;">Plymouth LETS</h3></a>
    </div>

    <div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
					<li><a href="postAdd.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post an Ad</h4></a></li>
					<li><a href="postRequest.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post a Request</h4></a></li>
			</ul>
			<div class="col-sm-3" style="margin-left:200px;">
				<form class="navbar-form" method= "post" style="width:300px; margin-top:25px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>			
					</div>			
					<!--<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Category/subCategory...
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
								<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
								<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
						</ul>
					</div>-->
			
				</form>
			</div>
			<ul class="nav navbar-nav" style="float:right;">
					<li><a href=""> </a></li>
					<li><a href=""></a></li>
					<li><a href=""> </a></li>
					<li><a href="account.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Your Account</h4></a></li>
					<li><a href=""><h4 style="font-weight:bold; color:white; margin-top:25px;">Sign Out</h4></a></li>
					<li><a href=""> </a></li>
					<li><a href=""></a></li>
					<li><a href=""> </a></li>
					<li><a href="register.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Register</h4></a> </li>
				</ul>
		</div>	
	
    </nav>
	

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
	  <br />
	  <h1>Your Account</h1><br />
		<p>This is your Account-Dashboard. From here you can view and edit your account details, view your transaction history, leave feedback for completed transactions and, if you really want to, remove your account.</p>
	  
		
</div>
    </div>
	
<div class="col-sm-3 col-md-2 sidebar" style=" height: 300px; text-align: right;">
         
          <ul class="nav nav-sidebar" style="font-size:20px;">
			<li><a href="account.php" style="font-weight:bold;">Account</a></li>
            <li><a href="" style="font-weight:bold;">Posts</a></li>
			<ul class="nav nav-sidebar">
			<li style= "padding-right: 5%;"><a href="yourAds.php">Ads</a></li>
            <li style= "padding-right: 5%;"><a href="yourRequests.php">Requests</a></li>
			
            <li><a href="swapHistory.php" style="font-weight:bold;">Swap History</a></li>
            <li style= "padding-right: 5%;"><a href="leaveFeedback.php">Leave Feedback</a></li>
            
          </ul>
     </div>
	<div class="container">
         <div class="row">
		<div class="col-sm-9">
		 <form>
			<div class='col-md-4'style = " text-align: left; ">
					<h1>Your Details</h1><br />
					
					<p style="font-size:18px;">Forename:</p>
					<p style = "font-weight: bold; font-size:16px;">John</p><br />
					<p style="font-size:18px;">Surname:</p>
					<p style = "font-weight: bold; font-size:16px;">Smith</p><br />
					<p style="font-size:18px;">Email address:</p>
					<p style = "font-weight: bold; font-size:16px;">jSmith@gmail.com</p><br />
			</div>
			<div class='col-md-4'style = " text-align: left; ">
					<h1 style ="color: white;">Your Details</h1><br />
					
					<p style="font-size:18px;">Username:</p>
					<p style = " font-weight: bold; font-size:16px;">jSmith91</p><br />
					<p style="font-size:18px;">City:</p>
					<p style = "font-weight: bold; font-size:16px;">Plymouth</p><br />
					<p style="font-size:18px;">Postcode:</p>
					<p style = "font-weight: bold; font-size:16px;">PL4 6GJ</p><br />
			</div>
			<div class='col-md-4'style = " text-align: left; ">
					<h1 style ="color: white;">Your Details</h1><br />
					
					<p style="font-size:18px;">Contact Number:</p>
					<p style = " font-weight: bold; font-size:16px;">01234567891</p><br />
					<p style="font-size:18px;">Alternative Number:</p>
					<p style = "font-weight: bold; font-size:16px;">07895476234</p><br />
					<p style="font-size:18px;">Swap Balance:</p>
					<p style = "font-weight: bold; font-size:16px;">5</p>
					
					<a class="btn btn-default" href="account.php" role="button">Edit Your Details »</a>
			</div>
		</form>
		</div>
			
        
      </div>
  
    
</div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./index_files/jquery.min.js"></script>
    <script src="./index_files/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./index_files/ie10-viewport-bug-workaround.js"></script>
  

</body></html>
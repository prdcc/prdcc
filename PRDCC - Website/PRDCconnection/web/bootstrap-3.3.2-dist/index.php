
<!DOCTYPE html>

<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>Plymouth LETS</title>

    <!-- Bootstrap core CSS -->
	<!--<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="css\bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" rel="home" href="/" title="Plymouth LETS"><h3 style="color:white;">Plymouth LETS</h3></a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
					<li><a href="postAdd.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post an Ad</h4></a></li>
					<li><a href="postRequest.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post a Request</h4></a></li>
			</ul>
			<div class="col-sm-3" style="margin-left:200px;">
				<form class="navbar-form" method= "post" style="width:300px; margin-top:25px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>			
					</div>			
					<!--<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Category/subCategory...
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
						</ul>
					</div>-->
			
				</form>
			</div>
			<ul class="nav navbar-nav" style="float:right;">
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="account.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Your Account</h4></a></li>
				<li><a href=""><h4 style="font-weight:bold; color:white; margin-top:25px;">Sign Out</h4></a></li>
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="register.php"><h4 style="font-weight:bold; color:white; margin-top:25px;">Register</h4></a> </li>
			</ul>
		</div>	
    </nav>
	

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
  <div id="Carousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#Carousel" data-slide-to="0" class="active"></li>
      <li data-target="#Carousel" data-slide-to="1"></li>
      <li data-target="#Carousel" data-slide-to="2"></li>
     
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="  alt="Chania" style="width: 150px; height: 150px; float: left; margin-left: 10%; margin-right: 10%;"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="  alt="Chania" style="width: 150px; height: 150px; float: left; margin-left: 10%; margin-right: 10%;"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="  alt="Chania" style="width: 150px; height: 150px; float: left; margin-left: 10%; margin-right: 10%;">
	</div>

      <div class="item">
        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="  alt="Chania" style="width: 150px; height: 150px; float: left; margin-left: 10%; margin-right: 10%;"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="  alt="Chania" style="width: 150px; height: 150px; float: left; margin-left: 10%; margin-right: 10%;"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="  alt="Chania" style="width: 150px; height: 150px; float: left; margin-left: 10%; margin-right: 10%;">
	</div>
    
      <div class="item">
        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="  alt="Chania" style="width: 150px; height: 150px; float: left; margin-left: 10%; margin-right: 10%;"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="  alt="Chania" style="width: 150px; height: 150px; float: left; margin-left: 10%; margin-right: 10%;"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="  alt="Chania" style="width: 150px; height: 150px; float: left; margin-left: 10%; margin-right: 10%;">
	</div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#Carousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#Carousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
    </div>

	<div class="container">
    <h1 style="font-weight:bold;">Swaps Currency</h1>
	<p style="font-size:18px;">The currency that we use is Swaps. Swaps are gained through trading services or products.
	   Items are charged at a one-off price. Services are charged at an hourly rate.</p>
	   <br>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active">
          <a href="#ads" role="tab" data-toggle="tab" style="font-size:28px;">
              <i class="fa fa-user"></i> Ads
          </a>
      </li>
      <li><a href="#requests" role="tab" data-toggle="tab" style="font-size:28px;">
          <i class="fa fa-user"></i> Requests
          </a>
      </li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane fade active in" id="ads">
         <div class="row">
		 <?php 
		 if (isset($_POST['srch-term']))
			{
				echo 
				"<div class='col-md-4'>
					<h2>Maths Tutoring</h2>
					<a href='viewAdd.php'>
						<img src='http://cliparts.co/cliparts/6cp/Lrg/6cpLrgyEi.jpg' class='img-circle' alt='Generic placeholder image' style='width: 140px; height: 140px;'>
					</a>
					<p>I'm an A-level maths teacher at fakeSchool, offering maths tutoring at all level up to and incluing key stage 5.</p>
					<p><a class='btn btn-default' href='viewAdd.php' role='button'>View details »</a></p>
				</div>";
			}
			else
			{
				echo 
				"<div class='col-md-4'>
					<h2>Dog Walking</h2><img class='img-circle' src='https://d13yacurqjgara.cloudfront.net/users/1960/screenshots/309611/doggy.jpg' alt='Generic placeholder image' style='width: 140px; height: 140px;'>
					<p>I'm offering a dog walking service to any dog-owning maths teachers who would be willing to provide tutoring for my maths GCSE.</p>
					<p><a class='btn btn-default' href='http://getbootstrap.com/examples/jumbotron/#' role='button'>View details »</a></p>
				</div>
				<div class='col-md-4'>
					<h2>Baby-gro's 18mths</h2><img class='img-circle' src='http://www.tractorted.co.uk/prodimg/BABYGROWFM_1_Zoom.jpg' alt='Generic placeholder image' style='width: 140px; height: 140px;'>
					<p>I'm getting rid of my baby grows (aged 18mths) as my little one has grown too big for them!.</p>
					<p><a class='btn btn-default' href='http://getbootstrap.com/examples/jumbotron/#' role='button'>View details »</a></p>
				</div>
				<div class='col-md-4'>
					<h2>Web Designer</h2><img class='img-circle' src='http://www.webdesignhighwycombe.com/images/internet-marketing-services.jpg' alt='Generic placeholder image' style='width: 140px; height: 140px;'>
					<p>I'm looking to set up a small website for my photography business. I need a small site to display my photos and allow people to contact me.</p>
					<p><a class='btn btn-default' href='http://getbootstrap.com/examples/jumbotron/#' role='button'>View details »</a></p>
				</div>
				<div class='col-md-4'>
					<h2>Maths Tutoring</h2>
					<a href='viewAdd.php'>
						<img class='img-circle' src='http://cliparts.co/cliparts/6cp/Lrg/6cpLrgyEi.jpg' alt='Generic placeholder image' style='width: 140px; height: 140px;'>
					</a>
					<p>I'm an A-level maths teacher at fakeSchool, offering maths tutoring at all level up to and incluing key stage 5.</p>
					<p><a class='btn btn-default' href='viewAdd.php' role='button'>View details »</a></p>
				</div>";
			} ?>
			
        
      </div>
      </div>
      <div class="tab-pane fade" id="requests">
          <div class="row">
        <div class="col-md-4">
          <h2>Heading</h2><img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <p>I offer a weekly grass cutting service on saturdays. 5swaps a session.</p>
		  <p><a class="btn btn-default" href="http://getbootstrap.com/examples/jumbotron/#" role="button">View details »</a></p>
        </div>
        <div class="col-md-4">
          <h2>Heading</h2><img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="http://getbootstrap.com/examples/jumbotron/#" role="button">View details »</a></p>
       </div>
        <div class="col-md-4">
          <h2>Heading</h2><img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="http://getbootstrap.com/examples/jumbotron/#" role="button">View details »</a></p>
        </div>
		<div class="col-md-4">
          <h2>Heading</h2><img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="http://getbootstrap.com/examples/jumbotron/#" role="button">View details »</a></p>
        </div>
		<div class="col-md-4">
          <h2>Heading</h2><img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="http://getbootstrap.com/examples/jumbotron/#" role="button">View details »</a></p>
        </div>
		<div class="col-md-4">
          <h2>Heading</h2><img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="http://getbootstrap.com/examples/jumbotron/#" role="button">View details »</a></p>
        </div>
      </div>
      </div>
  
    </div>
    
</div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./index_files/jquery.min.js"></script>
    <script src="./index_files/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./index_files/ie10-viewport-bug-workaround.js"></script>
  

</body></html>
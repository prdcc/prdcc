<!DOCTYPE html>
<%@page import="java.util.ArrayList"%>
<%@page import = "java.sql.*" %>
<%@page import = "java.sql.Connection" %>
<%@page import = "java.sql.DriverManager" %>
<%@page import = "java.sql.SQLException" %>

<%@page import = "user.User" %>
<%@page import = "database.Database2" %>


<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>PlymLETS: Your Account</title>

    <!-- Bootstrap core CSS -->
	<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css\bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" rel="home" href="index.jsp" title="Plymouth LETS"><img src="logo-new.png"></a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
					<li><a href="postAdd.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post an Ad</h4></a></li>
					<li><a href="postRequest.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post a Request</h4></a></li>
			</ul>
			<div class="col-sm-3" style="margin-left:200px;">
				<form action = "index.jsp" class="navbar-form" method="post" style="width:300px; margin-top:25px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="searchKey" id="searchKey">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>
					</div>	
                                </form>
					<!--<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Category/subCategory...
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
						</ul>
					</div>-->
			
				
			</div>
			<ul class="nav navbar-nav" style="float:right;">
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="account.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Your Account</h4></a></li>
				
                                            <%  if (session.getAttribute("logged_in") == null || (String) session.getAttribute("logged_in") == "false")
                                            {
                                                out.println("<li><a href='signIn.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign IN");
                                            }
                                            else if ((String) session.getAttribute("logged_in") == "true") {
                                                out.println("<li><a href='signOut.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign OUT, " + session.getAttribute("username"));
                                            }
                                            
                                            %></h4></a></li>
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="register.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Register</h4></a> </li>
			</ul>
		</div>	
    </nav>
	

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
	  <br />
	  <h1>Your Account</h1><br />
		<p>This is your Account-Dashboard. From here you can view and edit your account details, view your transaction history, leave feedback for completed transactions and, if you really want to, remove your account.</p>
	  
		
</div>
    </div>
	
<div class="col-sm-3 col-md-2 sidebar" style=" height: 300px; text-align: right;">
         
          <ul class="nav nav-sidebar" style="font-size:20px;">
			<li><a href="account.jsp" style="font-weight:bold;">Account</a></li>
            <li><a href="" style="font-weight:bold;">Posts</a></li>
			<ul class="nav nav-sidebar">
			<li style= "padding-right: 5%;"><a href="yourAds.jsp">Ads</a></li>
            <li style= "padding-right: 5%;"><a href="yourRequests.jsp">Requests</a></li>
			
            <li><a href="swapHistory.jsp" style="font-weight:bold;">Swap History</a></li>
            <li style= "padding-right: 5%;"><a href="leaveFeedback.jsp">Leave Feedback</a></li>
            <li><a href="messages.jsp" style="font-weight:bold;">Your Messages</a></li>
            
          </ul>
</div>
	<div class="container">
         <div class="row">
		<div class="col-sm-9">
                    <%
                        String forename = "NOT FOUND";
                        String surname = "NOT FOUND";
                        String email = "NOT FOUND";
                        String username = "NOT FOUND";
                        String city = "NOT FOUND";
                        String postcode = "NOT FOUND";
                        String phone = "NOT FOUND";
                        String balance = "NOT FOUND";
                        if (session.getAttribute("logged_in") == null || (String) session.getAttribute("logged_in") == "false") {
                            String site = "signIn.jsp";
                            response.setStatus(response.SC_MOVED_TEMPORARILY);
                            response.setHeader("Location", site);
                        } else if ((String) session.getAttribute("logged_in") == "true") {
                            ArrayList<User> userItem = User.findBySQL("SELECT * FROM PRDCC.USERS WHERE USERNAME = '" + session.getAttribute("username") + "'");
                            User user = userItem.get(0);
                            forename = user.getForename();
                            surname = user.getSurname();
                            email = user.getEmail();
                            username = user.getUsername();
                            city = user.getCity();
                            postcode = user.getPostcode();
                            phone = user.getPhone();
                            balance = Integer.toString(user.getBalance());
                        }
                        
                       
                        if (request.getParameter("edit")!=null && request.getParameter("edit").equals("true")){
                            out.println(
                            "<form action = 'updateUser.jsp' method = 'post'>"
                                + "<div class='col-md-4'style = ' text-align: left; '>"
                                    + "<h1>Your Details</h1><br />"
                                            +"<div class='input-group' style = 'text-align: center;'>"
                                                + "<p style='font-size:18px;'>Forename:</p>"
                                                    +"<input type='text' class='form-control' placeholder='Forename' required = 'required' maxlength = '15' pattern = '^[a-zA-Z]+$' title ='Only alphabetical characters' name='forename' id='forename' value =" + forename + " style = 'width: 100%; font-size:16px;'><p style = 'color: white;'>spacing</p>"
                                                + "<p style='font-size:18px;'>Surname:</p>"    
                                                    +"<input type='text' class='form-control' placeholder='Surname' required = 'required' maxlength = '15' pattern = '^[a-zA-Z]+$' title ='Only alphabetical characters' name='surname' id='surname' value ="+surname+" style = 'width: 100%; font-size:16px;'><p style = 'color: white;'>spacing</p>"
                                                + "<p style='font-size:18px;'>Email address:</p>"   
                                                    +"<input type='email' class='form-control' required = 'required' maxlength = '100' placeholder='Email' name='email' id='email' value =" + email + " style = 'width: 100%; font-size:16px;'><p style = 'color: white;'>spacing</p>"
                                            +"</div>"

                                + "</div>"
                                + "<div class='col-md-4'style = ' text-align: left; '>"
                                    + "<h1 style ='color: white;'>Your Details</h1><br />"
                                            +"<div class='input-group' style = 'text-align: center;'>"
                                                + "<p style='font-size:18px;'>Username:</p>"
                                                     + "<p style = 'font-weight: bold; font-size:16px;'>" + username + "</p><p style = 'color: white;'>spacing</p>"
                                                + "<p style='font-size:18px;'>City:</p>"    
                                                    +"<input type='text' class='form-control' placeholder='City' required = 'required' maxlength = '15' pattern = '^[a-zA-Z]+$' title ='Only alphabetical characters' name='city' id='city' value ="+city+" style = 'width: 100%; font-size:16px;'><p style = 'color: white;'>spacing</p>"
                                                + "<p style='font-size:18px;'>Postcode:</p>"   
                                                    +"<input type='text' class='form-control' placeholder='Postcode' required = 'required' maxlength = '8' pattern = 'PL[0-9]{1,2}\\s?[0-9]{1,1}[A-Z]{2,2}$|pl[0-9]{1,2}\\s?[0-9]{1,1}[a-z]{2,2}$' title = 'Only Plymouth postcodes' name='postcode' id='postcode' value =" + postcode + " style = 'width: 100%; font-size:16px;'><p style = 'color: white;'>spacing</p>"
                                            +"</div>"
                                + "</div>"
                                + "<div class='col-md-4'style = ' text-align: left; '>"
                                    + "<h1 style ='color: white;'>Your Details</h1><br />"
                                            +"<div class='input-group' style = 'text-align: center;'>"
                                                + "<p style='font-size:18px;'>Contact Number:</p>"
                                                    +"<input type='text' class='form-control' placeholder='Phone' pattern ='^\\s*\\(?(020[7,8]{1}\\)?[ ]?[1-9]{1}[0-9{2}[ ]?[0-9]{4})|(0[1-8]{1}[0-9]{3}\\)?[ ]?[1-9]{1}[0-9]{2}[ ]?[0-9]{3})\\s*$' title = 'Valid UK phone number. Landlines: 0xxxx xxxxxx. Mobiles: 07xxx xxxxxx' required = 'required' maxlength = '13' name='phone' id='phone' value =" + phone + " style = 'width: 100%; font-size:16px;'><p style = 'color: white;'>spacing</p>"
                                                + "<p style='font-size:18px;'>Swap Balance:</p>"  
                                                    + "<p style = 'font-weight: bold; font-size:16px;'>" + balance + "</p>"
                                            +"</div>"                                       
                                + "</div>"
                                +" <button class='btn-lg btn-success' type='submit' href=''>Save</button>"
                                +"<a class='btn-lg' href='account.jsp' role='button'>Cancel</a>"
                            + "</form>");

                        } else{
                            out.println(
                            "<form>"
                                + "<div class='col-md-4'style = ' text-align: left; '>"
                                    + "<h1>Your Details</h1><br />"
                                        + "<p style='font-size:18px;'>Forename:</p>"
                                        + "<p style = 'font-weight: bold; font-size:16px;'>" + forename + "</p><br />"
                                        + "<p style='font-size:18px;'>Surname:</p>"
                                        + "<p style = 'font-weight: bold; font-size:16px;'>" + surname + "</p><br />"
                                        + "<p style='font-size:18px;'>Email address:</p>"
                                        + "<p style = 'font-weight: bold; font-size:16px;'>" + email + "</p><br />"
                                + "</div>"
                                + "<div class='col-md-4'style = ' text-align: left; '>"
                                    + "<h1 style ='color: white;'>Your Details</h1><br />"
                                        + "<p style='font-size:18px;'>Username</p>"
                                        + "<p style = ' font-weight: bold; font-size:16px;'>" + username + "</p><br />"
                                        + "<p style='font-size:18px;'>City:</p>"
                                        + "<p style = 'font-weight: bold; font-size:16px;'>" + city + "</p><br />"
                                        + "<p style='font-size:18px;'>Postcode:</p>"
                                        + "<p style = 'font-weight: bold; font-size:16px;'>" + postcode + "</p><br />"
                                + "</div>"
                                + "<div class='col-md-4'style = ' text-align: left; '>"
                                    + "<h1 style ='color: white;'>Your Details</h1><br />"
                                        + "<p style='font-size:18px;'>Contact Number:</p>"
                                        + "<p style = ' font-weight: bold; font-size:16px;'>" + phone + "</p><br />"
                                        
                                        + "<p style='font-size:18px;'>Swap Balance:</p>"
                                        + "<p style = 'font-weight: bold; font-size:16px;'>" + balance + "</p>"
                                        + "<a class='btn btn-default' href='account.jsp?edit=true' role='button'>Edit Your Details �</a>"
                                + "</div>"
                            + "</form>");
                        }
                    %>
		
		</div>
			
        
      </div>
  
    
</div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./index_files/jquery.min.js"></script>
    <script src="./index_files/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./index_files/ie10-viewport-bug-workaround.js"></script>
  

</body></html>
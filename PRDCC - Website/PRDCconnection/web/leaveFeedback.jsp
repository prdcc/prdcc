<%@page import="java.util.ArrayList"%>
<%@page import = "java.sql.*" %>
<%@page import = "java.sql.Connection" %>
<%@page import = "java.sql.DriverManager" %>
<%@page import = "java.sql.SQLException" %>

<%@page import = "ad.Ad" %>
<%@page import = "feedback.Feedback" %>
<%@page import = "ad.Archive" %>
<%@page import = "user.User" %>
<%@page import = "database.Database2" %>
<!DOCTYPE html>
<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<%
    ArrayList<Feedback> feedbacksB = Feedback.findBySQL("SELECT ID, ARCHIVE FROM PRDCC.FEDBACK WHERE NVL(RATING, 0) = 0 "
            + "AND ARCHIVE IN (SELECT ID FROM ARCHIVE WHERE BUYER = '"+session.getAttribute("username")+"')");
    
     ArrayList<Feedback> feedbacksS = Feedback.findBySQL("SELECT ID, ARCHIVE FROM PRDCC.FEDBACK WHERE NVL(RATING, 0) = 0 "
            + "AND ARCHIVE IN (SELECT ID FROM ARCHIVE WHERE SELLER = '"+session.getAttribute("username")+"')");

%>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="http://getbootstrap.com/favicon.ico">

        <title>PlymLETS: Your Account</title>

        <!-- Bootstrap core CSS -->
        <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="css\bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>


    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" rel="home" href="index.jsp" title="Plymouth LETS"><img src="logo-new.png"></a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
					<li><a href="postAdd.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post an Ad</h4></a></li>
					<li><a href="postRequest.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post a Request</h4></a></li>
			</ul>
			<div class="col-sm-3" style="margin-left:200px;">
				<form action = "index.jsp" class="navbar-form" method="post" style="width:300px; margin-top:25px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="searchKey" id="searchKey">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>
					</div>	
                                </form>
					<!--<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Category/subCategory...
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
						</ul>
					</div>-->
			
				
			</div>
			<ul class="nav navbar-nav" style="float:right;">
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="account.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Your Account</h4></a></li>
				
                                            <%  if (session.getAttribute("logged_in") == null || (String) session.getAttribute("logged_in") == "false")
                                            {
                                                out.println("<li><a href='signIn.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign IN");
                                            }
                                            else if ((String) session.getAttribute("logged_in") == "true") {
                                                out.println("<li><a href='signOut.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign OUT, " + session.getAttribute("username"));
                                            }
                                            
                                            %></h4></a></li>
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="register.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Register</h4></a> </li>
			</ul>
		</div>	
    </nav>


        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">

            <div class="container">
                <br />

                <h1>Leave Feedback</h1><br />
                <p>This is the Leave Feedback page. From here you can leave feedback for all the swaps you have completed. This feedback will effect the other member's rating.</p>

            </div>
        </div>
	
        <div class="col-sm-3 col-md-2 sidebar" style=" height: 300px; text-align: right;">

            <ul class="nav nav-sidebar" style="font-size:20px;">
                <li><a href="account.jsp" style="font-weight:bold;">Account</a></li>
                <li><a href="" style="font-weight:bold;">Posts</a></li>
                <ul class="nav nav-sidebar">
                    <li style= "padding-right: 5%;"><a href="yourAds.jsp">Ads</a></li>
                    <li style= "padding-right: 5%;"><a href="yourRequests.jsp">Requests</a></li>
                </ul>       
                    <li><a href="swapHistory.jsp" style="font-weight:bold;">Swap History</a></li>
                    <li style= "padding-right: 5%;"><a href="leaveFeedback.jsp">Leave Feedback</a></li>
                    <li><a href="messages.jsp" style="font-weight:bold;">Your Messages</a></li>

            </ul>
        </div>



        <div class="container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" style="height: 42px;">
                <li class="active">
                    <a href="#ads" role="tab" data-toggle="tab" style="font-size:16px;">
                        <i class="fa fa-user"></i> Things you've bought
                    </a>
                </li>
                <li><a href="#requests" role="tab" data-toggle="tab" style="font-size:16px;">
                        <i class="fa fa-user"></i> Things you've sold
                    </a>
                </li>
            </ul>


            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane fade active in" id="ads">
                    <%
                        if (feedbacksB.isEmpty()){
                            out.println("<p style='margin: 5%;'>You are up to date on all feedback as a buyer. </p>");
                        }
                        else {
                            for (Feedback fb : feedbacksB){
                                Archive theAd = (Archive) Archive.findBySQL("SELECT * FROM PRDCC.ARCHIVE WHERE ID = "+ fb.getArchiveId()).get(0);
                                out.println("<div class='row'>"
                                                +"<div class='col-md-4'style = 'width: 100%; text-align: left;'>"
                                                    +"<h2>Feedback for: "+theAd.getSellerId()+"</h2>"
                                                    +"<h3>As seller of: "+theAd.getTitle()+"</h3><img class='img-circle' src='ad.png' alt='Generic placeholder image' style='width: 140px; height: 140px;'><p style = 'color: white;'>spacing</p>"
                                                    +"<form action = 'postFeedback.jsp?id="+fb.getId()+"' method='post'>"
                                                        +"<div class='input-group' style = 'text-align: center;'>"
                                                            +"<input type='text' class='form-control' required = 'required' maxlength = '200' pattern = '^[a-zA-Z]+$' title = 'Only alphabetical characters' placeholder='Comments' name='comment' id='comment' style = 'width: 140%; font-size:16px;'><p style = 'color: white;'>spacing</p>"
                                                            +"<input type='text' required = 'required' maxlength = '1' pattern = '^[0-9]+$' title =  'Only numerical characters' class='form-control' placeholder='Rating - 1 (bad) - 5 (excellent)' name='rating' id='rating' style = 'width: 140%; font-size:16px;'><p style = 'color: white;'>spacing</p>"
                                                        +"</div>"
                                                        +"<p><button type='submit' class='btn btn-default' role='button' style='font-size:16px;'>Leave feedback</button></p>"
                                                     +"</form>"
                                                +"</div>"
                                            +"</div>");
                            }
                        }
                    %>
                </div>
                
                <div class="tab-pane fade" id="requests">
                    <%
                        if (feedbacksS.isEmpty()){
                            out.println("<p style='margin: 5%;'>You are up to date on all feedback as a seller. </p>");
                        }
                        else {
                            for (Feedback fb : feedbacksS){
                                Archive theAd = (Archive) Archive.findBySQL("SELECT * FROM PRDCC.ARCHIVE WHERE ID = "+ fb.getArchiveId()).get(0);
                                out.println("<div class='row'>"
                                                +"<div class='col-md-4'style = 'width: 100%; text-align: left;'>"
                                                    +"<h2>Feedback for: "+theAd.getBuyerId()+"</h2>"
                                                    +"<h3>As buyer of: "+theAd.getTitle()+"</h3><img class='img-circle' src='req.png' alt='Generic placeholder image' style='width: 140px; height: 140px;'><p style = 'color: white;'>spacing</p>"
                                                    +"<form action = 'postFeedback.jsp?id="+fb.getId()+"' method='post'>"
                                                        +"<div class='input-group' style = 'text-align: center;'>"
                                                            +"<input type='text' class='form-control' required = 'required' maxlength = '200' pattern = '^[a-zA-Z]+$' title = 'Only alphabetical characters' placeholder='Comments' name='comment' id='comment' style = 'width: 140%; font-size:16px;'><p style = 'color: white;'>spacing</p>"
                                                            +"<input type='text' class='form-control' required = 'required' maxlength = '1' pattern = '^[0-9]+$' title =  'Only numerical characters' placeholder='Rating - 1 (bad) - 5 (excellent)' name='rating' id='rating' style = 'width: 140%; font-size:16px;'><p style = 'color: white;'>spacing</p>"
                                                        +"</div>"
                                                        +"<p><button type='submit' class='btn btn-default' role='button' style='font-size:16px;'>Leave feedback</button></p>"
                                                     +"</form>"
                                                +"</div>"
                                            +"</div>");
                            }
                        }
                    %>
                </div>

            </div>


        </div>



        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="./index_files/jquery.min.js"></script>
        <script src="./index_files/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="./index_files/ie10-viewport-bug-workaround.js"></script>


    </body></html>
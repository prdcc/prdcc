<!DOCTYPE html>
<%@page import="java.util.ArrayList"%>
<%@page import = "java.sql.*" %>
<%@page import = "java.sql.Connection" %>
<%@page import = "java.sql.DriverManager" %>
<%@page import = "java.sql.SQLException" %>

<%@page import = "user.User" %>
<%@page import = "database.Database2" %>

<%
    if (request.getParameter("username")!=null && request.getParameter("forename")!=null && request.getParameter("surname")!=null && request.getParameter("email")!=null && request.getParameter("city")!=null && 
                    request.getParameter("postcode")!=null && request.getParameter("phone")!=null && request.getParameter("uni")!=null && request.getParameter("password")!=null){
            User newUser = new User(request.getParameter("username"), request.getParameter("forename"), request.getParameter("surname"), request.getParameter("email"), request.getParameter("city"), 
                    request.getParameter("postcode"),request.getParameter("phone"), request.getParameter("uni"), request.getParameter("password"), 30);       
            newUser.create();

             User added = null; 
             try {
                added = (User) User.findBySQL("SELECT * FROM PRDCC.USERS WHERE USERNAME ='" + request.getParameter("username") + "'").get(0);
                session.setAttribute("username", added.getUsername());
                session.setAttribute("logged_in", "true");
                String site = "index.jsp" ;
                response.setStatus(response.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", site);
             } catch (java.lang.IndexOutOfBoundsException e)
             {
                String site = "index.jsp" ;
                response.setStatus(response.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", site);
             }

    }
    
    
        //out.println(res.getString(0));
    
%>

<!-- saved from url=(0043)http://getbootstrap.com/examples/jumbotron/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>Plymouth LETS</title>

    <!-- Bootstrap core CSS -->
	<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="css\bootstrap.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.2.min.js"></script>
    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/jumbotron/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./index_files/ie-emulation-modes-warning.js"></script><style type="text/css"></style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" rel="home" href="index.jsp" title="Plymouth LETS"><img src="logo-new.png"></a>
		</div>

		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
					<li><a href="postAdd.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post an Ad</h4></a></li>
					<li><a href="postRequest.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Post a Request</h4></a></li>
			</ul>
			<div class="col-sm-3" style="margin-left:200px;">
				<form action = "index.jsp" class="navbar-form" method="post" style="width:300px; margin-top:25px;">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="searchKey" id="searchKey">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>
					</div>	
                                </form>
					<!--<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Category/subCategory...
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Products</a></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Services</a></li>
						</ul>
					</div>-->
			
				
			</div>
			<ul class="nav navbar-nav" style="float:right;">
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="account.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Your Account</h4></a></li>
				
                                            <%  if (session.getAttribute("logged_in") == null || (String) session.getAttribute("logged_in") == "false")
                                            {
                                                out.println("<li><a href='signIn.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign IN");
                                            }
                                            else if ((String) session.getAttribute("logged_in") == "true") {
                                                out.println("<li><a href='signOut.jsp'><h4 style='font-weight:bold; color:white; margin-top:25px;'>Sign OUT, " + session.getAttribute("username"));
                                            }
                                            
                                            %></h4></a></li>
				<li><a href=""> </a></li>
				<li><a href=""></a></li>
				<li><a href=""> </a></li>
				<li><a href="register.jsp"><h4 style="font-weight:bold; color:white; margin-top:25px;">Register</h4></a> </li>
			</ul>
		</div>	
    </nav>
	

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
	  <br />
		<h1 style="text-align: center;">Register an Account </h1><br />
		<p>Register an account with us today to begin swapping with other members and help build a more connected Plymouth community.</p>
</div>
    </div>
	

	<div class="container">
    
   
    
    <!-- Tab panes -->
    
      
         <div class="row">
             <form action ="register.jsp" method="post">
             <div class='col-md-4' style = "width: 45%; text-align: center; margin-top: 2%; margin: 0px 10% 0% 0px; ">
                 <h3>Personal Information</h3>
                 
                     <div class="input-group" style = "text-align: center;">
                         <input type="text" class="form-control" placeholder="Forename" required = "required" maxlength = "15" pattern = "^[a-zA-Z]+$" title = "Only alphabetical characters"name="forename" id="forename" style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>
                         <input type="text" class="form-control" placeholder="Surname" required = "required"maxlength = "15" pattern = "^[a-zA-Z]+$" title = "Only alphabetical characters"name="surname" id="surname"style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>
                         <input type="email" onChange = "checkEmailMatch();"class="form-control" placeholder="Email" required = "required" maxlength = "150" title = "Valid email addresses" name="email" id="email"style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>
                         <input type="email" onChange = "checkEmailMatch();"class="form-control" placeholder="Confirm Email" required = "required" maxlength = "150" title = "Valid email address that matches" name="confirm_email" id="confirm_email"style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>
                         <div style = "font-size: 18px; color: RED; margin-top: 4px;"id="divCheckEmailMatch"></div>
                     </div>
                 
             </div>
             <div class='col-md-4' style = "width: 45%; text-align: center; margin-top: 2%; margin: 0px 0% 0px 0px; ">
                 <h3 style= "text-align: center;">Address Information</h3>
                  
                     <div class="input-group">
                         <input type="text" class="form-control" placeholder="City" required = "required" maxlength = "30" pattern = "^[a-zA-Z]+$" title = "Only alphabetical characters" name="city" id="city" style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>
                         <input type="text" class="form-control" placeholder="Postcode" required = "required" maxlength = "8" pattern = "PL[0-9]{1,2}\s?[0-9]{1,1}[A-Z]{2,2}$|pl[0-9]{1,2}\s?[0-9]{1,1}[a-z]{2,2}$" title = "Only Plymouth postcodes"name="postcode" id="postcode"style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>
                         <input type="text" class="form-control" placeholder="Uni"  required = "required" maxlength = "30" pattern = "^[a-zA-Z]+$" title = "Only alphabetical characters" name="uni" id="uni"style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>

                     </div>
               
             </div>

             <div class='col-md-4' style = "width: 45%; text-align: center; margin-top: 2%; margin: 0px 10% 0% 0px; ">
                 <h3>Account Information</h3>
                  
                     <div class="input-group">
                         <input type="text" class="form-control" placeholder="Username" required = "required" maxlength = "20" pattern = "^[a-zA-Z0-9_]*$" title = "Only alphanumeric characters" name="username" id="username" style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>
                         <input type="password" class="form-control" onChange = "checkPasswordMatch();" placeholder="Password" name="password" required = "required" maxlength = "50"id="password"style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>
                         <input type="password" class="form-control" onChange = "checkPasswordMatch();" placeholder="Confirm Password" name="confirm_password" required = "required" maxlength = "50"id="confirm_password"style = "width: 250%; font-size:16px;">
                         <div style = "font-size: 18px; color: RED; margin-top: 4px;"id="divCheckPasswordMatch"></div>
                     </div>
                
             </div>

             <div class='col-md-4' style = "width: 45%; text-align: center; margin-top: 2%; margin: 0px 0% 0px 0px; ">
                 <h3 style= "text-align: center;">Contact Information</h3>
                  
                     <div class="input-group">
                         <input type="text" class="form-control" placeholder="Phone Number" name="phone" pattern ="^\s*\(?(020[7,8]{1}\)?[ ]?[1-9]{1}[0-9{2}[ ]?[0-9]{4})|(0[1-8]{1}[0-9]{3}\)?[ ]?[1-9]{1}[0-9]{2}[ ]?[0-9]{3})\s*$" title = "Valid UK phone number. Landlines: 0xxxx xxxxxx. Mobiles: 07xxx xxxxxx"required = "required" maxlength = "13"id="phone" style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>
                         <input type="text" class="form-control" placeholder="Alternative Number" pattern ="^\s*\(?(020[7,8]{1}\)?[ ]?[1-9]{1}[0-9{2}[ ]?[0-9]{4})|(0[1-8]{1}[0-9]{3}\)?[ ]?[1-9]{1}[0-9]{2}[ ]?[0-9]{3})\s*$" title = "Valid UK phone number beginning with 0 (for landlines) or 07(for mobiles)"required = "required" maxlength = "13"name="alt_number" id="alt_number"style = "width: 250%; font-size:16px;"><p style = "color: white;">spacing</p>
                     </div>
                  

             </div>
                 <div style = "color: white;">fdsfs</div>
                <button class="btn-lg btn-success" type="submit" data-toggle='modal' data-target ='#confirm'>Register</button>
                <a href ="index.jsp" class="btn-lg btn-default" type="buton">Cancel</a>
                 </form>
             <script>
                function checkPasswordMatch() {
                    var password = $("#password").val();
                    var confirmPassword = $("#confirm_password").val();

                    if (password !== confirmPassword)
                        $("#divCheckPasswordMatch").html("Passwords do not match!");
                    else
                        $("#divCheckPasswordMatch").html("");
}
        </script>
        <script>
                function checkEmailMatch() {
                    var password = $("#email").val();
                    var confirmPassword = $("#confirm_email").val();

                    if (password !== confirmPassword)
                        $("#divCheckEmailMatch").html("Emails do not match!");
                    else
                        $("#divCheckEmailMatch").html("");
}
        </script>

			
		
			
        
      </div>
  
    
</div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./index_files/jquery.min.js"></script>
    <script src="./index_files/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./index_files/ie10-viewport-bug-workaround.js"></script>
  

</body></html>
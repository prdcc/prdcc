import database.Database2;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;


public class Ad {
    public static Database2 database = new Database2();
    public int id;
    public String title;
    public int price;
    public String description;
    public String category;
    public String subcategory;
    public int sellerId;
    public String status;
    public String datePosted;
    public String endDate;
    public String city;
    public String postcode;
    public int distance;
    
    public Ad(int id, String title, int price, String description, String category,
        String subcategory, int sellerId, String status, String datePosted, String endDate,
        String city, String postcode, int distance){
            this.id = id;
            this.title = title;
            this.price = price;
            this.description = description;
            this.category = category;
            this.subcategory = subcategory;
            this.sellerId = sellerId;
            this.status = status;
            this.datePosted = datePosted;
            this.endDate = endDate;
            this.city = city;
            this.postcode = postcode;
            this.distance = distance;
    }
    
    public void create(){
        String sql = "INSERT INTO ADS (ID, TITLE, PRICE, DESCRIPTION, CATEGORY, SUBCATEGORY, SELLER, STATUS, START-DATE, END-DATE, CITY, POSTCODE, DESCRIPTION) ";
        sql += " VALUES (" ; 
        sql += Integer.toString(this.id);
        sql += ", " + this.title;
        sql += ", " + Integer.toString(this.price);
        sql += ", " + this.description;
        sql += ", " + this.category;
        sql += ", " + this.subcategory;
        sql += ", " + Integer.toString(this.sellerId);
        sql += ", " + this.status;
        sql += ", " + this.datePosted;
        sql += ", " + this.endDate;
        sql += ", " + this.city;
        sql += ", " + this.postcode;
        sql += ", " + Integer.toString(distance);
        sql += ");";
        
        database.query(database.prepare(sql));
    } 
    
    public void delete(){
        String sql = "DELETE FROM ADS WHERE ID = " + Integer.toString(this.id);
        database.query(database.prepare(sql));
    }
    
    public static ArrayList findBySQL(String sql){

        ResultSet resultSet = database.query(database.prepare(sql));
        ArrayList ads = new ArrayList();
        try{
            while (resultSet.next()){
                Ad newAd = new Ad(resultSet.getInt(1), resultSet.getString(2),resultSet.getInt(3),resultSet.getString(4),resultSet.getString(5),resultSet.getString(6),resultSet.getInt(7),
                resultSet.getString(8),resultSet.getString(9),resultSet.getString(10),resultSet.getString(11),resultSet.getString(12),resultSet.getInt(13));
                ads.add(newAd);
            }
        } catch (SQLException e){}
        return ads;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public int getSellerId() {
        return sellerId;
    }

    public String getStatus() {
        return status;
    }

    public String getDatePosted() {
        return datePosted;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public int getDistance() {
        return distance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDatePosted(String datePosted) {
        this.datePosted = datePosted;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
    
    
    
}

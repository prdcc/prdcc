/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pagination;

import static java.lang.Math.ceil;

/**
 *
 * @author Richard
 */
public class Pagination {
    public int currentPage;
    public int perPage;
    public int totalRecs;
    
    
    public Pagination(int currentPage, int perPage, int totalRecs){
        this.currentPage = currentPage;
        this.perPage = perPage;
        this.totalRecs = totalRecs;
    }
    
	public int offset(){
		return (this.currentPage-1)*this.perPage;
	}
	
	public int totalPages(){
		return (int) (Math.ceil((this.totalRecs/this.perPage + 0.1)));
	}
	
	public int next_page(){
		if (this.has_next()){
			return this.currentPage +1;
		}
		else{
			return this.currentPage;
		}
	}
	
	public int prev_page(){
		if (this.has_prev()){
			return this.currentPage -1;
		}
		else{
			return this.currentPage;
		}
	}
	
	public boolean has_prev(){
		return this.currentPage > 1;
	}
	
	public boolean has_next(){
		return this.currentPage < this.totalPages();
	}
}

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    public Connection connection = null;
    
    public Database(){
        this.openConnection();
    }
    
    public void openConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:oracle:thin:@tom.uopnet.plymouth.ac.uk:1522:ORCL12c",
                    "PRDCC", "Plymouth2014!"); //Change to match your database credentials
        } catch (SQLException ex) {
        }
        
    }
    
    public PreparedStatement prepare(String statement) {
        PreparedStatement preparedStatement = null;
        try {
            connection.prepareStatement(statement);
        } catch (SQLException ex) {
        }

        return preparedStatement;
    }

    public ResultSet query(PreparedStatement preparedStatement) {
        ResultSet results = null;
        try {
            results = preparedStatement.executeQuery();
        } catch (SQLException ex) {
        }

        return results;
    }

    public Database database = new Database();
}


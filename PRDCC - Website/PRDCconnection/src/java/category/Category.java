/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package category;
import database.Database2;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author Richard
 */
public class Category {
    String category;
    ArrayList<String> subCats;
    
    public Category(String category){
        this.category = category;
        this.subCats = new ArrayList<String>();
    }
    
    public static ArrayList findBySQL(String sql){
        Database2 database = new Database2();
        database.openConnection();
        ResultSet resultSet = database.query(database.prepare(sql));
        ArrayList<Category> cats = new ArrayList<Category>();
        try{
            while (resultSet.next()){
                Category newCat = new Category(resultSet.getString(1));
                newCat.getSubs();
                cats.add(newCat);
            }
        } catch (SQLException e){}
        return cats;
    }
    
     public static ArrayList findAll(){
        String sql = "SELECT * FROM PRDCC.CATEGORYS";
        Database2 database = new Database2();
        database.openConnection();
        ResultSet resultSet = database.query(database.prepare(sql));
        ArrayList<Category> cats = new ArrayList<Category>();
        try{
            while (resultSet.next()){
                Category newCat = new Category(resultSet.getString(1));
                newCat.getSubs();
                cats.add(newCat);
            }
        } catch (SQLException e){}
        return cats;
    }

    public String getCategory() {
        return category;
    }

    public ArrayList<String> getSubCats() {
        return subCats;
    }
    
    
     
     public void getSubs(){
        Database2 database = new Database2();
        database.openConnection();
        String sql = "SELECT * FROM PRDCC.SUBCATEGORIES WHERE CATEGORYS = '" + this.getCategory() + "'";
        ResultSet resultSet = database.query(database.prepare(sql));
       
        try{
            while (resultSet.next()){
               
                this.subCats.add(resultSet.getString(1));
            }
        } catch (SQLException e){}
       
     }
}

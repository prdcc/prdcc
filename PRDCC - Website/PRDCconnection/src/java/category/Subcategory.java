/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package category;
import database.Database2;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author Richard
 */
public class Subcategory {
    String subcategory;
    
    public Subcategory(String subcategory){
        this.subcategory = subcategory;
    }
    
    public static ArrayList findBySQL(String sql){
        Database2 database = new Database2();
        database.openConnection();
        ResultSet resultSet = database.query(database.prepare(sql));
        ArrayList<Subcategory> subcats = new ArrayList<Subcategory>();
        try{
            while (resultSet.next()){
                Subcategory newCat = new Subcategory(resultSet.getString(1));
                subcats.add(newCat);
            }
        } catch (SQLException e){}
        return subcats;
    }
    
     public static ArrayList findAll(){
        String sql = "SELECT * FROM PRDCC.SUBCATEGORIES";
                Database2 database = new Database2();
        database.openConnection();
        ResultSet resultSet = database.query(database.prepare(sql));
        ArrayList<Subcategory> subcats = new ArrayList<Subcategory>();
        try{
            while (resultSet.next()){
                Subcategory newCat = new Subcategory(resultSet.getString(1));
                subcats.add(newCat);
            }
        } catch (SQLException e){}
        return subcats;
    }
}

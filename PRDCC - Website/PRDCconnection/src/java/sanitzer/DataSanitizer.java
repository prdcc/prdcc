/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sanitzer;
import java.util.regex.Pattern;
import database.Database2;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Richard
 */
public class DataSanitizer {
    
    public boolean isAlphabetical(String toCheck){
        return Pattern.matches("^[a-zA-Z]*$", toCheck);
    }
    
    public boolean isNumerical (String toCheck){
        return Pattern.matches("[0-9]+", toCheck);
    }
    
    public boolean isPlymPostcode(String toCheck){
        return Pattern.matches("PL[0-9]{1,2}\\s?[0-9]{1,1}[A-Z]{2,2}$|pl[0-9]{1,2}\\s?[0-9]{1,1}[a-z]{2,2}$", toCheck);
    }
    
    public boolean isEmail(String toCheck){
        return Pattern.matches("^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@(([0-9a-zA-Z])+([-\\w]*[0-9a-zA-Z])*\\.)+[a-zA-Z]{2,9})$", toCheck);
    }
    
    public boolean alreadyExists(String toCheck, String table, String checker, String value){
        Database2 database = new Database2();
        String sql="SELECT " + toCheck + " FROM PRDCC."+ table +" WHERE "+checker+" = '"+value+"'";
        ResultSet results = database.query(database.prepare(sql));
        try {
            //if the result set contains a record, the value checked already exists - return true
            results.next();
            return true;
        } catch (SQLException ex) {
            //if no record found in result set, value checked doesn't exist and is okay to use - return false
            return false;
        }
        
    }
}

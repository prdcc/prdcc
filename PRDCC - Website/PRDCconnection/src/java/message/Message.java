/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package message;
import database.Database2;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;


public class Message {
    
    public int id;
    public String senderId;
    public String receiverId;
    public String subject;
    public String body;
    public String read;
    
    public Message(int id, String senderId, String receiverId, String subject, String body, String read){
            this.id = id;
            this.senderId = senderId;
            this.receiverId = receiverId;
            this.subject = subject;
            this.body = body;
            this.read = read;
    }
    
    
    private static int newID(Database2 database){
        int id = 0;
        String sql = "SELECT ID FROM (SELECT ID, RANK() OVER (ORDER BY ID DESC) ID_RANK FROM PRDCC.MESSAGES) WHERE ID_RANK <=1";
        ResultSet lastId = database.query(database.prepare(sql));
        try{
            while(lastId.next()){
                id = Integer.parseInt(lastId.getString(1));
                id ++;
                return id;
            }
        } catch (SQLException e){
        
        }
        return id;
    }
    
    public void create(){
        Database2 database = new Database2();
        this.id = Message.newID(database);
        String sql = "INSERT INTO PRDCC.MESSAGES (ID, SENDER, RECEIVER, SUBJECT, BODY, READ) VALUES (" ; 
        sql += Integer.toString(this.id) + ", '";
        sql += this.senderId + "', '";
        sql += this.receiverId + "', '";
        sql += this.subject + "', '";
        sql +=  this.body + "', '";
        sql += this.read + "')";
        
        database.query(database.prepare(sql));
    } 
    
    public void delete(){
        Database2 database = new Database2();
        String sql = "DELETE FROM PRDCC.MESSAGES WHERE ID = " + Integer.toString(this.id);
        database.query(database.prepare(sql));
    }
    
    public static int countUnread(String receiver){
        int unread =0;
        Database2 database = new Database2();
        ResultSet results = database.query(database.prepare("SELECT COUNT(ID) FROM PRDCC.MESSAGES WHERE READ = 'false' AND RECEIVER = '" + receiver + "'"));
        try{
            while (results.next()){
                unread = results.getInt(1);
                return unread;
            }
        } catch (SQLException e){}
        return unread;
    }
    
    public void markAsRead(){
        Database2 database = new Database2();
        this.read = "true";
        database.query(database.prepare("UPDATE PRDCC.MESSAGES SET READ = 'true' WHERE ID = " + Integer.toString(this.id)));
    }
    
    public void markAsUnread(){
        Database2 database = new Database2();
        this.read = "true";
        database.query(database.prepare("UPDATE PRDCC.MESSAGES SET READ = 'false' WHERE ID = " + Integer.toString(this.id)));
    }
    
    public static ArrayList findBySQL(String sql){
        Database2 database = new Database2();
        ResultSet results = database.query(database.prepare(sql));
        ArrayList<Message> messages = new ArrayList<Message>();
        try{
            while (results.next()){
                //id,datsold,buyerid,title,price,desc,category,subcat,seller,dateposted,enddate,city,postcode,distance
                Message newMsg = new Message(results.getInt(1), results.getString(2), results.getString(3), results.getString(4),results.getString(5), results.getString(6));
                messages.add(newMsg);
            }
        } catch (SQLException e){}
        return messages;
    }

    public int getId() {
        return id;
    }

    public String getSenderId() {
        return senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public String getRead() {
        return read;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setRead(String read) {
        this.read = read;
    }
    
}

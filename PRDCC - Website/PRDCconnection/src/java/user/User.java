/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import database.Database2;
import static java.lang.System.out;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;


public class User {
    
    public String username;
    public String forename;
    public String surname;
    public String email;
    public String city;
    public String postcode;
    public String phone;
    public String uni;
    public String password;
    public int balance;
    
    public User(String username, String forename, String surname, String email, String city,
        String postcode, String phone, String uni, String password, int balance){
            this.username = username;
            this.forename = forename;
            this.surname = surname;
            this.email = email;
            this.city = city;
            this.postcode = postcode;
            this.phone = phone;
            this.uni = uni;
            this.password = password;
            this.balance = balance;
    }
    
    public void create(){
        Database2 database = new Database2();
        String sql = "INSERT INTO PRDCC.USERS (USERNAME, FIRSTNAME, SURNAME, EMAIL, CITY, POSTCODE, CONTACTNUMBER, UNI, PASSWORD, BALANCE, STATUS) VALUES ('";
        sql += this.username;
        sql += "', '" + this.forename;
        sql += "', '" + this.surname;
        sql += "', '" + this.email;
        sql += "', '" + this.city;
        sql += "', '" + this.postcode;
        sql += "', " + this.phone;
        sql += ", '" + this.uni;
        sql += "', '" + this.password;
        sql += "', " + Integer.toString(this.balance);
        sql += ", 'ACTIVE')";
        database.query(database.prepare(sql));
    } 
    
    public void update(){
        Database2 database = new Database2();
        String sql = "UPDATE PRDCC.USERS SET ";
        sql += "USERNAME = '"+this.username;
        sql += "', FIRSTNAME = '" + this.forename;
        sql += "', SURNAME = '" + this.surname;
        sql += "', EMAIL ='" + this.email;
        sql += "', CITY = '" + this.city;
        sql += "', POSTCODE = '" + this.postcode;
        sql += "', CONTACTNUMBER = " + this.phone;
        sql += ", UNI = '" + this.uni;
        sql += "', PASSWORD = '" + this.password;
        sql += "', BALANCE = " + Integer.toString(this.balance);
        sql += " WHERE USERNAME = '" + this.username + "'";
        database.query(database.prepare(sql));
    }
    
    public void delete(){
        Database2 database = new Database2();
        String sql = "DELETE FROM USERS.REQUESTS WHERE USERNAME = '" + this.username + "'";
        database.query(database.prepare(sql));
    }
    
    public static ArrayList findBySQL(String sql){
        Database2 database = new Database2();
        database.openConnection();
        ResultSet resultSet = database.query(database.prepare(sql));
        ArrayList<User> users = new ArrayList<User>();
        try{
            while (resultSet.next()){
                User newReq = new User(resultSet.getString(1), resultSet.getString(2),resultSet.getString(3),resultSet.getString(4),resultSet.getString(5),resultSet.getString(6),resultSet.getString(7),
                resultSet.getString(8),resultSet.getString(9),resultSet.getInt(10));
                users.add(newReq);
            }
        } catch (SQLException e){}
        return users;
    }

    public String getUsername() {
        return username;
    }

    public String getForename() {
        return forename;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getPhone() {
        return phone;
    }

    public String getUni() {
        return uni;
    }

    public String getPassword() {
        return password;
    }

    public int getBalance() {
        return balance;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUni(String uni) {
        this.uni = uni;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    
    
    
}


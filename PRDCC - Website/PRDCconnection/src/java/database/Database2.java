package database;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Database2 {
    public Connection connection = null;
    
    public Database2(){
        this.openConnection();
    }
    
    public String openConnection() {
        try {
            this.connection = DriverManager.getConnection("jdbc:oracle:thin:@tom.uopnet.plymouth.ac.uk:1522:ORCL12c",
                    "PRDCC", "Plymouth2014!"); 
            return "done";
        } catch (SQLException ex) {
            return ex.getMessage();
        }
        
    }
    
    public PreparedStatement prepare(String statement) {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = this.connection.prepareStatement(statement);
        } catch (SQLException ex) {
        }

        return preparedStatement;
    }

    public ResultSet query(PreparedStatement preparedStatement) {
        ResultSet results = null;
        try {
            results = preparedStatement.executeQuery();
        } catch (SQLException ex) {
        }

        return results;
    }

//    public Database2 database = new Database2();
}

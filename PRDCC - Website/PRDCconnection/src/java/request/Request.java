/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package request;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import ad.Ad;
import database.Database2;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Request {
    
    public int id;
    public String title;
    public int price;
    public String description;
    public String category;
    public String subcategory;
    public String requesterId;
    public String status;
    public String datePosted;
    public String neededDate;
    public String city;
    public String postcode;
    public int distance;
    public String offer;
    
    public Request(int id, String title, int price, String description, String category,
        String subcategory, String requesterId, String status, String datePosted, String neededDate,
        String city, String postcode, int distance, String offer){
            this.id = id;
            this.title = title;
            this.price = price;
            this.description = description;
            this.category = category;
            this.subcategory = subcategory;
            this.requesterId = requesterId;
            this.status = status;
            this.datePosted = datePosted;
            this.neededDate = neededDate;
            this.city = city;
            this.postcode = postcode;
            this.distance = distance;
            this.offer = offer;
    }
    
    public void create(){
        Database2 database = new Database2();
        this.id = this.newID(database);
        String sql = "INSERT INTO PRDCC.REQUESTS (ID, TITLE, PRICE, DESCRIPTION, CATEGORY, SUBCATAGORY, REQUESTER, STATUS, DATEPOSTED, DATENEEDED, CITY, POSTCODE, DISTANCE, OFFER) VALUES (" ; 
        sql += Integer.toString(this.id) + ", '";
        sql += this.title + "', ";
        sql +=  Integer.toString(this.price) + ", '";
        sql +=  this.description + "', '";
        sql +=  this.category + "', '";
        sql +=  this.subcategory + "', '";
        sql += this.requesterId + "', '";
        sql +=  this.status + "', ";
        sql += "TO_DATE('" + this.datePosted + "', 'mm/dd/yyyy')" + ", ";
        sql += "TO_DATE('" + this.neededDate + "', 'mm/dd/yyyy')" + ", '";
        sql += this.city + "', '";
        sql += this.postcode + "', ";
        sql += Integer.toString(this.distance) + ", '";
        sql += this.offer;
        sql += "')";
        
        database.query(database.prepare(sql));
    }
    
    public void update(){
        Database2 database = new Database2();
        String  sql = "UPDATE PRDCC.REQUESTS SET ";
        sql += "TITLE='" + this.title + "', PRICE=";
        sql +=  Integer.toString(this.price) + ", DESCRIPTION='";
        sql +=  this.description + "', CATEGORY='";
        sql +=  this.category + "', SUBCATAGORY='";
        sql +=  this.subcategory + "', STATUS='";
        sql +=  this.status + "', CITY='";
        sql += this.city + "', POSTCODE='";
        sql += this.postcode + "', DISTANCE=";
        sql += Integer.toString(this.distance) + ", OFFER='";
        sql += this.offer + "' ";
        sql += " WHERE ID="+Integer.toString(this.id);
        
        database.query(database.prepare(sql));
    }
    
    private int newID(Database2 database){
        int id = 0;
        String sql = "SELECT ID FROM (SELECT ID, RANK() OVER (ORDER BY ID DESC) ID_RANK FROM PRDCC.REQUESTS) WHERE ID_RANK <=1";
        ResultSet lastId = database.query(database.prepare(sql));
        try{
            while(lastId.next()){
                id = Integer.parseInt(lastId.getString(1));
                id ++;
                return id;
            }
        } catch (SQLException e){
        
        }
        return id;
    }
    
    public void delete(){
        Database2 database = new Database2();
        String sql = "DELETE FROM PRDCC.REQUESTS WHERE ID = " + Integer.toString(this.id);
        database.query(database.prepare(sql));
    }
    
    public static ArrayList findBySQL(String sql, boolean paged){
        Database2 database = new Database2();
        database.openConnection();
        ResultSet resultSet = database.query(database.prepare(sql));
        ArrayList<Request> requests = new ArrayList<Request>();
        try{
            while (resultSet.next()){
                if(!paged){
                Request newReq = new Request(resultSet.getInt(1), resultSet.getString(2),resultSet.getInt(3),resultSet.getString(4),resultSet.getString(5),resultSet.getString(6),resultSet.getString(7),
                resultSet.getString(8),resultSet.getString(9),resultSet.getString(10),resultSet.getString(11),resultSet.getString(12),resultSet.getInt(13), resultSet.getString(14));
                requests.add(newReq);} 
                else{Request newReq = new Request(resultSet.getInt(1+1), resultSet.getString(2+1),resultSet.getInt(3+1),resultSet.getString(4+1),resultSet.getString(5+1),resultSet.getString(6+1),resultSet.getString(7+1),
                resultSet.getString(8+1),resultSet.getString(9+1),resultSet.getString(10+1),resultSet.getString(11+1),resultSet.getString(12+1),resultSet.getInt(13+1), resultSet.getString(14+1));
                requests.add(newReq);}
            }
        } catch (SQLException e){}
        return requests;
    }
    
    public static int countRows(){
        Database2 database = new Database2();
        database.openConnection();
        ResultSet resultSet = database.query(database.prepare("SELECT COUNT(ID) FROM PRDCC.REQUESTS WHERE STATUS = 'APPROVED'"));
        try {
            while (resultSet.next()){
                return resultSet.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Ad.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return 0;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public String getRequesterId() {
        return requesterId;
    }

    public String getStatus() {
        return status;
    }

    public String getDatePosted() {
        return datePosted;
    }

    public String getNeededDate() {
        return neededDate;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public int getDistance() {
        return distance;
    }

    public String getOffer() {
        return offer;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public void setRequesterId(String requesterId) {
        this.requesterId = requesterId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDatePosted(String datePosted) {
        this.datePosted = datePosted;
    }

    public void setNeededDate(String neededDate) {
        this.neededDate = neededDate;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }
    
    
    
}

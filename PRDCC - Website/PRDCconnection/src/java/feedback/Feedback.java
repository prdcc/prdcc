/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feedback;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import ad.Ad;
import database.Database2;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Feedback {
    
    public int id;
    public int archiveId;
    public String dateLeft;
    public int rating;
    public String comment;
    
    public Feedback(int id, int archiveId, String dateLeft, int rating, String comment){
            this.id = id;
            this.archiveId = archiveId;
            this.dateLeft = dateLeft;
            this.rating = rating;
            this.comment = comment;
    }
    
    public void create(){
        Database2 database = new Database2();
        this.id = this.newID(database);
        String sql = "INSERT INTO PRDCC.FEDBACK (ID, ARCHIVE) VALUES (" ; 
        sql += Integer.toString(this.id) + ", ";
        sql +=  Integer.toString(this.archiveId);
        sql += ")";
        
        database.query(database.prepare(sql));
    }
    
     public void update(){
        Database2 database = new Database2();
        String sql = "UPDATE PRDCC.FEDBACK SET " ; 
        sql += "DATELEFT = TO_DATE('" + this.dateLeft + "', 'mm/dd/yyyy')" + ", ";
        sql +=  "RATING = " + Integer.toString(this.rating) + ", ";
        sql += "COMMENTS = '" + this.comment;
        sql += "' WHERE ID = " + this.id;
        
        database.query(database.prepare(sql));
    }
    
    private int newID(Database2 database){
        int id = 0;
        String sql = "SELECT ID FROM (SELECT ID, RANK() OVER (ORDER BY ID DESC) ID_RANK FROM PRDCC.FEDBACK) WHERE ID_RANK <=1";
        ResultSet lastId = database.query(database.prepare(sql));
        try{
            while(lastId.next()){
                id = Integer.parseInt(lastId.getString(1));
                id ++;
                return id;
            }
        } catch (SQLException e){
        
        }
        return id;
    }
    
    public void delete(){
        Database2 database = new Database2();
        String sql = "DELETE FROM PRDCC.REQUESTS WHERE ID = " + Integer.toString(this.id);
        database.query(database.prepare(sql));
    }
    
    public static int count(String sql){
        Database2 database = new Database2();
        database.openConnection();
        ResultSet resultSet = database.query(database.prepare(sql));
        try {
            while (resultSet.next()){
                return resultSet.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Ad.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return 0;
    }
    
    public static ArrayList findBySQL(String sql){
        Database2 database = new Database2();
        database.openConnection();
        ResultSet resultSet = database.query(database.prepare(sql));
        ArrayList<Feedback> scores = new ArrayList<Feedback>();
        try{
            while (resultSet.next()){
                Feedback newFeedback = new Feedback(resultSet.getInt(1), resultSet.getInt(2), "",0,"" );
                scores.add(newFeedback);
            }
        } catch (SQLException e){}
        return scores;
    }

    public int getId() {
        return id;
    }

    public int getArchiveId() {
        return archiveId;
    }

    public String getDateLeft() {
        return dateLeft;
    }

    public int getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setArchiveId(int archiveId) {
        this.archiveId = archiveId;
    }

    public void setDateLeft(String dateLeft) {
        this.dateLeft = dateLeft;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
    
}

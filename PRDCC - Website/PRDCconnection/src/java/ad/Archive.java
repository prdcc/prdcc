/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ad;
import database.Database2;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import feedback.Feedback;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Archive {
    
    public int id;
    public String dateSold;
    public String buyerId;
    public String title;
    public int price;
    public String description;
    public String category;
    public String subcategory;
    public String sellerId;
    public String datePosted;
    public String endDate;
    public String city;
    public String postcode;
    public int distance;
    
    public Archive(int id, String dateSold, String buyerId, String title, int price, String description, String category,
        String subcategory, String sellerId, String datePosted, String endDate,
        String city, String postcode, int distance){
            this.id = id;
            this.dateSold = dateSold;
            this.buyerId = buyerId;
            this.title = title;
            this.price = price;
            this.description = description;
            this.category = category;
            this.subcategory = subcategory;
            this.sellerId = sellerId;
            this.datePosted = datePosted;
            this.endDate = endDate;
            this.city = city;
            this.postcode = postcode;
            this.distance = distance;
    }
    
    public void create(){
        Database2 database = new Database2();
        this.id = this.newID(database);
        
        Feedback newFed = new Feedback(0, this.id, null, 0, null);
        newFed.create();
        
        String sql = "INSERT INTO PRDCC.ARCHIVE (ID, DATESOLD, BUYER, TITLE, PRICE, DESCRIPTION, CATEGORY, SUBCATAGORY, SELLER, DATEPOSTED, DATEEND, CITY, POSTCODE, DISTANCE) VALUES (" ; 
        sql += Integer.toString(this.id) + ", ";
        sql += "TO_DATE('" + this.dateSold + "', 'mm/dd/yyyy')" + ", '";
        sql += this.buyerId + "', '";
        sql += this.title + "', ";
        sql +=  Integer.toString(this.price) + ", '";
        sql += this.description + "', '";
        sql +=  this.category + "', '";
        sql +=  this.subcategory + "', '";
        sql += this.sellerId + "', ";
        sql += "TO_DATE('" + this.datePosted + "', 'mm/dd/yyyy')" + ", ";
        sql += "TO_DATE('" + this.endDate + "', 'mm/dd/yyyy')" + ", '";
        sql += this.city + "', '";
        sql += this.postcode + "', ";
        sql += Integer.toString(this.distance);
        sql += ")";
        
        database.query(database.prepare(sql));
    } 
    
    public static int count(String sql){
        Database2 database = new Database2();
        database.openConnection();
        ResultSet resultSet = database.query(database.prepare(sql));
        try {
            while (resultSet.next()){
                return resultSet.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Ad.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return 0;
    }
    
    private int newID(Database2 database){
        int id = 0;
        String sql = "SELECT ID FROM (SELECT ID, RANK() OVER (ORDER BY ID DESC) ID_RANK FROM PRDCC.ARCHIVE) WHERE ID_RANK <=1";
        ResultSet lastId = database.query(database.prepare(sql));
        try{
            while(lastId.next()){
                id = Integer.parseInt(lastId.getString(1));
                id ++;
                return id;
            }
        } catch (SQLException e){
        
        }
        return id;
    }
    
    public void delete(){
        Database2 database = new Database2();
        String sql = "DELETE FROM PRDCC.AD WHERE ID = " + Integer.toString(this.id);
        database.query(database.prepare(sql));
    }
    
    public static ArrayList findBySQL(String sql){
        Database2 database = new Database2();
        ResultSet results = database.query(database.prepare(sql));
        ArrayList<Archive> archives = new ArrayList<Archive>();
        try{
            while (results.next()){
                //id,datsold,buyerid,title,price,desc,category,subcat,seller,dateposted,enddate,city,postcode,distance
                Archive newArch = new Archive(results.getInt(1), results.getString(13), results.getString(14), results.getString(2), results.getInt(3), results.getString(4),results.getString(5),results.getString(6),results.getString(7),
                results.getString(8), results.getString(9), results.getString(10), results.getString(11), results.getInt(12));
                archives.add(newArch);
            }
        } catch (SQLException e){}
        return archives;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getDateSold() {
        return dateSold;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public String getCategory() {
        return category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public String getSellerId() {
        return sellerId;
    }

    public String getDatePosted() {
        return datePosted;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public int getDistance() {
        return distance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public void setDateSold(String dateSold) {
        this.dateSold = dateSold;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public void setDatePosted(String datePosted) {
        this.datePosted = datePosted;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
    
    
    
}
